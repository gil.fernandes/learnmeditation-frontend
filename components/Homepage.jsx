/* /components/Layout.js */

import React from 'react';
import Link from "next/link";

import {DropdownItem, NavItem} from "reactstrap";

import {CourseContext, SET_CURRENT_SECTION_INDEX} from '../context/CourseContext';
import {coursesFetch} from "../lib/learnMeditationClientFetch";
import {PageHeader} from "./PageHeader";
import {withTranslation} from 'react-i18next';
import '../lib/i18n';
import {LessonSlider} from "./homePage/LessonSlider";
import {MeditateNow} from "./homePage/MeditateNow";
import LanguageSelector from "./homePage/LanguageSelector";
import {MeditationResources} from "./homePage/MeditationResources";
import {Testimonials} from "./homePage/Testimonials";
import MenuHeader from "./menus/MenuHeader";
import TagLine from "./homePage/TagLine";
import Footer from "./footer/Footer";
import SocialIcons from "./SocialIcons";

/**
 * The actual layout component which is a wrapper around the page content.
 */
class Homepage extends React.Component {

    static contextType = CourseContext;

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {
            setAllCourses
        } = this.context;
        coursesFetch().then(courses => {
            setAllCourses(courses);
        })

    }

    static async getInitialProps({}) {
        let pageProps = {};
        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }
        return {pageProps};
    }

    setLanguage = (e) => {
        const lng = e.target.value;
        const {i18n} = this.props;
        i18n.changeLanguage(lng);
    }

    render() {
        const {
            courseSections
        } = this.context;
        const {i18n, t} = this.props;
        const title = t("Learn Meditation Online");
        const description = t("Meta description");
        return (
            <>
                <PageHeader description={description} title={title}/>
                <section id="hero" data-panel="hero" className="active">
                    <div className="content imgLiquid imgLiquid_bgSize imgLiquid_ready">
                        <div className="top-br">
                            <div className="container">
                                <div className="header-right wow fadeInRight animated" data-wow-delay="1s">
                                    <div className="row">
                                        <div className="col-lg-12 tp-res">
                                            <div className="row">
                                                <div className="col-lg-6 col-md-6 col-sm-6 col-6 tp-rs1"
                                                     id="languageSelection">
                                                    <LanguageSelector />
                                                </div>
                                                <div className="col-lg-6 col-md-6 col-sm-6 col-6 social-list tp-rs2">
                                                    <SocialIcons />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <TagLine />
                        <img src="/images/ban3.jpg" alt="Meditation" style={{display: "none"}}/>
                    </div>
                </section>

                <MenuHeader />

                <div className="about-med about-med1">
                    <div className="container">
                        <div className="wm-fancy-title wow zoomIn animated">
                            <img src="img/lotus.png" className="svg1" />
                                <h2>{t("About Meditation")}</h2>
                        </div>
                        <div className="container">
                            <div className="col-lg-12 ">
                                <div className="abt abt1 bg-white vid-cl">
                                    <div className="wm-ourvideo-wrap wm-ourvideo-wrap1 text-center des">
                                        <figure>
                                            <iframe width="500" height="400"
                                                    src={this.context.homepageVideoLink}
                                                    frameBorder="0" webkitallowfullscreen="true" mozallowfullscreen="true"
                                                    allowFullScreen />
                                        </figure>

                                    </div>
                                    <div className="wm-ourvideo-wrap wm-ourvideo-wrap1 text-center mobi">
                                        <iframe width="500" height="400" src={this.context.homepageVideoLink}
                                                frameBorder="0"
                                                webkitallowfullscreen="true" mozallowfullscreen="true" allowFullScreen />

                                    </div>
                                    <div className="abt-md abt-md-rs">
                                        <p>{t('intro-p1')}</p>
                                        <p>{t('intro-p2')}</p>
                                        <p>{t('intro-p3')}</p>
                                        <p>{t('intro-p4')}</p>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="about-med">
                    <div className="container">
                        <div className="wm-fancy-title wow zoomIn animated">
                            <img src="img/lotus.png" className="svg1" />
                                <h2>{t("slider-our-lessons")}</h2>
                        </div>
                        <div className="lsn text-center pl-0">
                            <h4 className="">{t("Our 7 Lessons Course")}</h4>
                        </div>

                    </div>
                </div>

                <LessonSlider />
                <MeditateNow />
                <MeditationResources />
                <Testimonials />
                <Footer />
            </>
        );
    }

    displayMeditateNowMusic(section) {
        if (section.modules.length > 0) {
            return (
                <ul>
                    {section.modules.map((m, i) => {
                        return (
                            <DropdownItem header key={i}>{m.audio.name}</DropdownItem>
                        )
                    })}
                </ul>
            )
        }
    }

    drawMeditateNow() {
        const {
            courseSections, meditateNowMeditations, selectedCourseId
        } = this.context;
        if (courseSections && meditateNowMeditations && meditateNowMeditations.length > 0) {
            return (
                <Link href="/meditateNow/[id]" as={`/meditateNow/${selectedCourseId}`}><a className="nav-link">Meditate
                    Now</a></Link>
            );
        }
    }

    redirectToLesson(i) {
        const {
            setCurrentModuleIndex, dispatch
        } = this.context;
        setCurrentModuleIndex(0);
        dispatch({type: SET_CURRENT_SECTION_INDEX, currentSectionIndex: i})
        const {pathname} = Router;
        if (pathname.indexOf('meditateNow')) {
            console.log(pathname, location.href);
            const m = location.href.match(/.+meditateNow\/(\d+)/);
            if (m && m.length > 0) {
                console.log('m[1]', m[1]);
                Router.push('/lessons/[id]', '/lessons/' + m[1]);
            }
        }
    }

    drawCurrentSections() {
        const {
            courseSections, lessonDropDownShow, setLessonDropDownShow
        } = this.context;
        if (courseSections) {
            return (
                <Dropdown nav isOpen={lessonDropDownShow}
                          toggle={() => setLessonDropDownShow(!lessonDropDownShow)} className="ml-auto">
                    <DropdownToggle nav caret>
                        Lessons
                    </DropdownToggle>
                    <DropdownMenu>
                        {courseSections.map((section, i) => {
                            return (
                                <DropdownItem onClick={() => this.redirectToLesson(i)}
                                              key={i}>{section.name}</DropdownItem>
                            )
                        })}
                    </DropdownMenu>
                </Dropdown>
            )
        }
    }
}

export default withTranslation()(Homepage);