import React from "react";
import {useTranslation} from "react-i18next";

/**
 * Image banner to be used on most pages.
 * @param bannerStyle The style of the banner.
 * @param titleKey The title i18n key
 * @returns {JSX.Element}
 * @constructor
 */
const ImageBanner = ({bannerStyle, titleKey}) => {
    const {t} = useTranslation();
    return (
        <div className={`wm-mini-header ${bannerStyle}`}>
            <span className="wm-black-transparent"/>
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="wm-page-heading">
                            <h1 className="w-color">{t(titleKey)}</h1>
                        </div>
                        <div className="clearfix"/>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ImageBanner