import React, {useState} from "react";
import AudioPlayerReact from "../audio/AudioPlayerReact";
import {Collapse} from "reactstrap";
import Accordion from "../Accordion";

/**
 * Accordion for meditate now.
 * @param items
 * @constructor
 */
export const MeditateNowAccordion = ({items}) => {

    const renderItems = (items, expandedItem, toggle) => (
        <div className="container-fluid meditate-now-container">
            <div id="accordion" className="accordion">
                {items.map((m, i) => {
                    const isExpanded = i === expandedItem
                    return (
                        <div className="card mb-0">
                            <div className={`card-header ${!isExpanded && 'collapsed'}`} data-toggle="collapse" href="#collapseOne"
                                 aria-expanded={isExpanded} onClick={e => toggle(i)}>
                                <a className="card-title" onClick={e => toggle(i)} aria-expanded={isExpanded}>
                                    {m.audio.caption || m.audio.name}
                                </a>
                            </div>
                            <Collapse isOpen={isExpanded}>
                                <div className="meditate-now-audio-wrapper">
                                    <AudioPlayerReact audio={m.audio} />
                                </div>
                            </Collapse>
                        </div>
                    )
                })}
            </div>
        </div>
    )

    return <Accordion items={items} renderItems={renderItems}/>
}