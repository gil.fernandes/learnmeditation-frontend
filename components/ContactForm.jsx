import {useContext, useRef, useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {
    CourseContext,
    SET_CONTACT_FORM_CLEAR_MESSAGES,
    SET_CONTACT_FORM_EMAIL,
    SET_CONTACT_FORM_ERROR_MESSAGE,
    SET_CONTACT_FORM_MESSAGE,
    SET_CONTACT_FORM_NAME,
    SET_CONTACT_FORM_SENDING,
    SET_CONTACT_FORM_SENDING_DONE,
    SET_CONTACT_FORM_SUCCESS_MESSAGE
} from "../context/CourseContext";
import {contactMailSend} from "../lib/mailSend";
import {getCurrentLanguage} from "../lib/langUtils";

const AlertMessage = ({type, message, dispatch}) => {
    return (
        <div className={`alert alert-${type}`} role="alert">
            {message}
            <button type="button" className="close" data-dismiss="alert" aria-label="Close" onClick={(e) => dispatch({type: SET_CONTACT_FORM_CLEAR_MESSAGES})}>
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    )
}

const languagesWithExtraMessage = ['fr']

/**
 * The actual contact form.
 * @returns {JSX.Element}
 * @constructor
 */
const ContactForm = () => {

    const {initialLanguage} = useContext(CourseContext)
    const [editorLoaded, setEditorLoaded] = useState(false)
    const {contactFormData, dispatchContactFormData} = useContext(CourseContext)
    const {i18n, t} = useTranslation();

    const emailRegex = /^[^\s@]{2,}@[^\s@]{2,}\.[^\s@]+$/
    const limit = 1024

    const editorRef = useRef()
    const { CKEditor, ClassicEditor } = editorRef.current || {}

    useEffect(() => {
        const ClassicEditor = require('@ckeditor/ckeditor5-build-classic')
        editorRef.current = {
            // CKEditor: require('@ckeditor/ckeditor5-react'), // depricated in v3
            CKEditor: require('@ckeditor/ckeditor5-react').CKEditor, // v3+
            ClassicEditor
        }
        // Editor configuration.
        ClassicEditor.defaultConfig = {
            toolbar: {
                items: [
                    'heading',
                    '|',
                    'bold',
                    'italic',
                    'link',
                    'bulletedList',
                    'numberedList',
                    '|',
                    'outdent',
                    'indent',
                    '|',
                    'blockQuote',
                    'insertTable',
                    'undo',
                    'redo'
                ]
            },
            table: {
                contentToolbar: [
                    'tableColumn',
                    'tableRow',
                    'mergeTableCells'
                ]
            },
            // This value must be kept in sync with the language defined in webpack.config.js.
            language: 'en'
        };
        setEditorLoaded(true)
    }, [])

    function isNameValid() {
        return contactFormData?.fullName.trim().length > 2;
    }

    function isEmailValid() {
        return emailRegex.test(contactFormData?.email.trim());
    }

    function isMessageValid() {
        return contactFormData?.message.trim().length > 10;
    }

    const isFormValid = () => {
        const nameValid = isNameValid()
        const messageValid = isMessageValid()
        const emailValid = isEmailValid()
        return nameValid && emailValid && messageValid
    }

    const onSubmitHandler = async (e) => {
        e.preventDefault()
        const currentLanguage = getCurrentLanguage(i18n);
        const submission = {
            "securityContactCode": "12291234123123123131312312213231",
            language: currentLanguage,
            ...contactFormData
        }
        console.log('submission', submission)
        try {
            dispatchContactFormData({type: SET_CONTACT_FORM_SENDING})
            const res = await contactMailSend(submission)
            dispatchContactFormData({type: SET_CONTACT_FORM_SENDING_DONE})
            console.log('submission result', res)
            if (res.status !== 200) {
                dispatchContactFormData({type: SET_CONTACT_FORM_ERROR_MESSAGE, message: t('contact-mail-failed')})
            } else {
                dispatchContactFormData({type: SET_CONTACT_FORM_SUCCESS_MESSAGE, message: t('contact-mail-success')})
            }
        } catch (e) {
            console.error('Contact mail error', e)
            setErrorMessage(e.message)
        }
    }

    console.log('initialLanguage', languagesWithExtraMessage.includes(initialLanguage))

    return (
        <div className="col-md-12 wm-contact-us-form-section">
            <div className="wm-contactus-title">
                <h2>{t('Contact Us Today')}</h2>
            </div>
            {languagesWithExtraMessage.includes(initialLanguage) && <div className="extra-contact-info" dangerouslySetInnerHTML={{__html: t('Contact Us Extra')}}/>}
            <div className="wm-contact-us-form-section">
                <form onSubmit={onSubmitHandler}>
                    <div className="form-group info-message">
                        {contactFormData?.errorMessage && <AlertMessage type="danger" message={contactFormData?.errorMessage} dispatch={dispatchContactFormData} />}
                        {contactFormData?.successMessage && <AlertMessage type="success" message={contactFormData?.successMessage} dispatch={dispatchContactFormData} />}
                    </div>
                    <div className="form-group">
                        <input type="text" className="form-control" value={contactFormData?.fullName}
                               placeholder={t('Your Name')}
                               onChange={e => dispatchContactFormData({
                                   type: SET_CONTACT_FORM_NAME,
                                   fullName: e.target.value
                               })}/>
                        {!isNameValid() &&
                        <small className="form-text text-muted">{t('hint-full-name', {amount_characters: 2})}</small>}
                    </div>
                    <div className="form-group">
                        <input type="email" className="form-control" value={contactFormData?.email}
                               placeholder={t('Your E-mail')}
                               onChange={e => dispatchContactFormData({
                                   type: SET_CONTACT_FORM_EMAIL,
                                   email: e.target.value
                               })}/>
                        {!isEmailValid() &&
                        <small className="form-text text-muted">{t('hint-email')}</small>}
                    </div>
                    <div className="form-group">
                        {editorLoaded && <CKEditor
                            editor={ClassicEditor}
                            data={contactFormData?.message}
                            onReady={editor => {
                                // You can store the "editor" and use when it is needed.
                                console.log('Editor is ready to use!', editor);
                            }}
                            onChange={(event, editor) => {
                                const data = editor.getData();
                                dispatchContactFormData({
                                    type: SET_CONTACT_FORM_MESSAGE,
                                    message: data
                                })
                            }}
                        />}
                        <div
                            className="message-size float-right">{`${contactFormData?.message.length} / ${limit}`}</div>
                        {!isMessageValid() &&
                        <small className="form-text text-muted">{t('hint-message', {amount_characters: 10})}</small>}

                    </div>
                    <div className="form-group">
                        <input type="submit" className="btn" value={t('Submit')} disabled={!isFormValid() || contactFormData?.sending}/>
                    </div>
                </form>
            </div>
        </div>
    )
}

export default ContactForm