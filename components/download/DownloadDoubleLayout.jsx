import {Row} from "reactstrap";
import DownloadAccordion from "./DownloadAccordion";
import NormalPageTemplate from "../NormalPageTemplate";
import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";

/**
 * Double column layout used in the audio section.
 * @constructor
 */
export const DownloadDoubleLayout = ({titleKey, bannerStyle, sections, renderSingleItem, icon = 'fa-volume-up'}) => {

    const [sectionsLeft, setSectionsLeft] = useState([])
    const [sectionsRight, setSectionsRight] = useState([])
    const {t} = useTranslation();

    useEffect(() => {
        if (sections) {
            const threshold = Math.ceil(sections.length / 2)
            setSectionsLeft(sections.slice(0, threshold))
            setSectionsRight(sections.slice(threshold))
        }
    }, [])


    return (
        <NormalPageTemplate titleKey={t(titleKey)} bannerStyle={bannerStyle}>
            <div className="container">
                {titleKey === 'Audio' && <h4 className="audioIntro">{t('download-audio-intro')}</h4>}
                <Row>
                    <div className="col-md-6">
                        <DownloadAccordion items={sectionsLeft} renderSingleItem={renderSingleItem} icon={icon}/>
                    </div>
                    <div className="col-md-6">
                        <DownloadAccordion items={sectionsRight} renderSingleItem={renderSingleItem} icon={icon}/>
                    </div>
                </Row>
            </div>
        </NormalPageTemplate>
    )
}

export default DownloadDoubleLayout