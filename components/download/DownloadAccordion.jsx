import {Collapse} from "reactstrap";
import Accordion from "../Accordion";
import React from "react";
import {targetServer} from "../../lib/apiConstants";

/**
 * Accordion for the download based on the simple accordion.
 * @param items The items to render.
 * @param renderSingleItem The function used to render a single item.
 * @param icon The icon to be displayed before the title
 * @constructor
 */
export const DownloadAccordion = ({items, renderSingleItem, icon = 'fa-volume-up'}) => {

    const renderItems = (items, expandedItem, toggle) => (
        <div className="container-fluid download-container">
            <div className="accordion">
                {items.map((item, i) => {
                    const isExpanded = i === expandedItem
                    return (
                        <div className="card mb-0" key={i}>
                            <div className={`card-header ${!isExpanded && 'collapsed'}`} data-toggle="collapse" href="#collapseOne"
                                 aria-expanded={isExpanded} onClick={() => toggle(i)}>
                                <a className="card-title" onClick={() => toggle(i)} aria-expanded={isExpanded}>
                                    <i className={`fa ${icon}`} />{' '}{item.name}
                                </a>
                            </div>
                            <Collapse isOpen={isExpanded}>
                                <div className="card-body">
                                    <ul className="list-style3">
                                    {renderSingleItem(item)}
                                    </ul>
                                </div>
                            </Collapse>
                        </div>
                    )
                })}
            </div>
        </div>
    )

    return <Accordion items={items} renderItems={renderItems}/>
}

export default DownloadAccordion