import Head from "next/head";
import React from "react";

/**
 * The page header with meta tags and
 * @param title The title of the page
 * @param description The description meta tag
 * @returns {JSX.Element}
 * @constructor
 */
export const PageHeader = ({title, description}) => {
    return (
        <Head>
            <title>{title}</title>
            <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
            <meta name="description" content={description} />
            <meta charSet="utf-8"/>
            <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
            {/* The favicon should be in the root directory */}
            <link rel="icon" href="/favicon.png" type="image/png" />
            <link href='https://fonts.googleapis.com/css?family=Rambla:400,700,400italic,700italic%7CLato:400,700%7CPoiret+One%7CTenor+Sans%7CJosefin+Sans:400,600,600italic%7CArizonia'
                  rel='stylesheet' type='text/css' />
            <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap"
                  rel="stylesheet" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />


        </Head>
    );
}