import React, {useContext, useEffect} from 'react';
import {useTranslation} from "react-i18next";
import {findCourseForLang} from "../../lib/langConfig";
import Link from "next/link";
import {CourseContext} from "../../context/CourseContext";
import {getCurrentLanguage} from "../../lib/langUtils";

/**
 * Meditate Now section in the homepage.
 */
export const MeditateNow = () => {
    const {initialLanguage, setInitialLanguage} = useContext(CourseContext)
    const {t, i18n} = useTranslation();

    useEffect(() => {
        setInitialLanguage(getCurrentLanguage(i18n))
    }, [])
    return (
        <div className="wm-main-section col-12 wm-ourservices-bg wm-ourservices-bg1 bg-bnr" id="meditate-now"
             data-panel="meditate-now">
            <div className="container">
                <div className="row">
                    <div className="col-md-12 wow fadeInLeft animated">
                        <div className="wm-ourservices-heading wm-ourservices-heading1">
                            <img src="img/lotusw.png" className="img-responsive wow zoomIn animated svg1"/>
                            <h5 className="bnr-img">{t('Meditate')}</h5>
                            <h5 className="md-now bnr-img">{t('Now')}</h5>
                            <Link href="/meditateNow/[id]" as={`/meditateNow/${findCourseForLang(initialLanguage)}`}>
                                <a className="wm-services-contact wm-services-contact1"
                                   href="meditate-now.html" data-panel="meditate-now">{t('Choose your Meditation')}</a>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}