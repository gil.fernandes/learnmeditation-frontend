import React, {useContext, useEffect, useState} from 'react';
import {useTranslation} from "react-i18next";
import {getCurrentLanguage} from "../../lib/langUtils";
import {CourseContext, SET_LANGUAGE} from "../../context/CourseContext";
import {LANGUAGES} from "../../lib/langConfig";


/**
 * Displays the language selection drop down.
 * @constructor
 */
const LanguageSelector = () => {
    const [initialLanguage, setInitialLanguage] = useState("en")
    const {i18n, t} = useTranslation();

    useEffect(() => {
        setInitialLanguage(getCurrentLanguage(i18n))
    }, [])

    const {dispatch} = useContext(CourseContext)

    const setLanguage = (e) => {
        const lng = e.target.value;
        switch (lng) {
            // case "de":
            //     location.href = 'http://www.online-meditieren-lernen.de/elearning/course/index?lang=de'
            //     return;
            case "ko":
                location.href = 'http://learnmeditationonline.org//elearning/course/courseStart/6?lang=ko'
                return;
            case "jp":
                location.href = 'http://learnmeditationonline.org//elearning/course/courseStart/7?lang=ja'
                return;
            // case "fr":
            //     location.href = 'http://apprendreamediter.fr/elearning/course/index?lang=fr'
            //     return;
        }
        i18n.changeLanguage(lng);
        dispatch({type: SET_LANGUAGE, currentLanguage: lng})
    }

    return (
        <select className="select-en"
                data-placeholder={t("Choose a Language ...")}
                title={t("Choose a Language ...")}
                onChange={setLanguage}
                value={i18n.language || initialLanguage}
        >
            {Object.values(LANGUAGES).map((l, i) =>
                (<option key={`${l.CODE}_${i}`} value={l.CODE}>{t(l.NAME)}</option>))}
        </select>
    )
}

export default LanguageSelector;