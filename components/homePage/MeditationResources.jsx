import React, {useContext, useEffect} from 'react'
import {useTranslation} from "react-i18next";
import {findCourseForLang, findGeneralFAQCategoryForLang} from "../../lib/langConfig";
import Link from "next/link";
import {getCurrentLanguage} from "../../lib/langUtils";
import {CourseContext} from "../../context/CourseContext";

/**
 * Meditation resources section on the front page.
 * @returns {JSX.Element}
 * @constructor
 */
export const MeditationResources = () => {
    const {initialLanguage, setInitialLanguage} = useContext(CourseContext)
    const {i18n, t} = useTranslation();
    const folder = 'audio'
    useEffect(() => {
        setInitialLanguage(getCurrentLanguage(i18n))
    }, [])
    return (
        <div className=" sect-padding sect-padding1 bg-color">
            <div className="container">
                <div className="container">
                    <div className="col-lg-12  abt abt1 abt2 abt2-1">
                        <div className="">
                            <div className="abt-md wow zoomIn animated">
                                <p>{t('meditation-resources-intro')}</p>
                            </div>

                            <div className="row mt-35">
                                <div
                                    className="col-lg-6 col-md-6 col-sm-6 text-center wow zoomInUp animated med-fq"
                                    data-wow-delay="0.1s">
                                    <div className="img-mn img-mn-mr1 ">
                                        <a href="#">
                                            <img src="img/1.jpg" className="img-fluid"/>
                                            <Link href={`/download/${folder}/[id]`}
                                                  as={`/download/${folder}/${findCourseForLang(initialLanguage)}`}>
                                                <div className="wm-event-title-caption text-img ">
                                                    <h2>{t('Meditation')}</h2>
                                                    <span>{t('Resources')}</span><br/>
                                                    <span>{t("Pdf's and Audio's")}</span>
                                                </div>
                                            </Link>
                                        </a>
                                    </div>
                                </div>
                                <div
                                    className="col-lg-6 col-md-6 col-sm-6 text-center wow zoomInUp animated med-fq"
                                    data-wow-delay="0.1s">
                                    <div className="img-mn img-mn-mr2 ">
                                        <a href="#">
                                            <img src="img/211.jpg" className="img-fluid"/>
                                            <Link href="/faqs/[id]" as={`/faqs/${findGeneralFAQCategoryForLang(initialLanguage)}`}>
                                                <div className="wm-event-title-caption text-img ">
                                                    <h2>{t('Meditation')}</h2>
                                                    <span>{t("FAQ's")}</span>
                                                </div>
                                            </Link>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}