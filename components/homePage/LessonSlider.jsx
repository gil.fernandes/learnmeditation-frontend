import React, {useContext, useEffect, useRef, useState} from "react";
import Slider from "react-slick";
import {useTranslation} from 'react-i18next';
import {CourseContext} from "../../context/CourseContext";
import {fetchLessonSlides} from "../../lib/lessonSliderFetch";
import {getCurrentLanguage} from "../../lib/langUtils";
import {LeftNavButton} from "../slider/LeftNavButton";
import {RightNavButton} from "../slider/RightNavButton";
import {findCourseForLang} from "../../lib/langConfig";
import Link from "next/link";

const MoreButton = () => {
    const {initialLanguage, setInitialLanguage} = useContext(CourseContext)
    const {t, i18n} = useTranslation();

    useEffect(() => {
        setInitialLanguage(getCurrentLanguage(i18n))
    }, [])

    return (
        <div className="row">
            <div className="col-12">
                <Link href="/lessons/[id]" as={`/lessons/${findCourseForLang(initialLanguage)}`} passHref>
                    <button className="btn-style11 know-more">{t('Know more')}</button>
                </Link>
            </div>
        </div>
    )
}

const SingleSlide = ({index, s}) => {
    const {i18n} = useTranslation()
    return (
        <div key={index}>
            <a href={`lessons/${findCourseForLang(getCurrentLanguage(i18n))}?lesson=${index + 1}`}>
                <figure>
                    <img src={s.img} alt={s.text} className="carrousel-image"/>
                    <figcaption><span className="lone">
                        <strong className="text-blue">{index + 1}</strong> {s.text}</span>
                    </figcaption>
                </figure>
            </a>
        </div>
    )
}

const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: false
};

const mobileSettings = {
    ...settings,
    slidesToShow: 1,
    slidesToScroll: 1
};

/**
 * Slider with the lessons
 * @returns {JSX.Element}
 * @constructor
 */
export const LessonSlider = () => {

    const slider = useRef(null)
    const mobileSlider = useRef(null)
    const {i18n} = useTranslation();
    const [slides, setSlides] = useState([])
    const getLanguage = () => getCurrentLanguage(i18n)
    const {courseLanguageData} = useContext(CourseContext)

    useEffect(() => {
        async function fetchData() {
            const s = await fetchLessonSlides(getLanguage())
            setSlides(s)
        }

        fetchData(); // No await
    }, [courseLanguageData?.currentLanguage])

    const drawSlides = (slides) => {
        return slides.map((s, index) => (
            <SingleSlide index={index} s={s} key={index}/>
        ))
    }

    return (
        <>
            <section id="brands" data-panel="brands" className="col-10 sec-lesson d-none d-lg-block">
                <article>
                    <Slider {...settings}
                            prevArrow={<LeftNavButton slider={slider}/>}
                            nextArrow={<RightNavButton slider={slider}/>} ref={slider}>
                        {drawSlides(slides)}
                    </Slider>
                </article>
                <MoreButton/>
            </section>
            <section id="brands" className="col-8 sec-lesson d-lg-none">
                <article>
                    <Slider {...mobileSettings}
                            prevArrow={<LeftNavButton slider={mobileSlider}/>}
                            nextArrow={<RightNavButton slider={mobileSlider}/>} ref={mobileSlider}>
                        {drawSlides(slides)}
                    </Slider>
                </article>
                <MoreButton/>
            </section>
        </>
    )

}