import React from 'react';
import {useTranslation} from "react-i18next";
import {getCurrentLanguage} from "../../lib/langUtils";
import {findCourseForLang, langConfig} from "../../lib/langConfig";
import {useRouter} from 'next/router'

/**
 * Tag line on the homepage with the button to the lessons page.
 * @returns {JSX.Element}
 * @constructor
 */
const TagLine = () => {
    const {i18n, t} = useTranslation();
    const router = useRouter()

    return (
        <div className="tagline">
            <h1 className="wow zoomIn animated tg-ln" data-wow-delay="1s">{t("Meditation")}</h1>
            <div className="col-md-12 col-sm-12 col-12">
                <div className="section-head wow fadeInUp animated" data-wow-delay="2s">
                    <h2 className="wow fadeInUp animated tg-ln2" data-wow-delay="2s">{t('tagline')}</h2>
                    <button className="btn-style12 btn-st11"
                            onClick={() => router.push(`/lessons/${findCourseForLang(getCurrentLanguage(i18n))}`)}>{t("Our lessons")}</button>
                </div>
            </div>
        </div>
    )
}

export default TagLine;