import React, {useContext, useEffect, useRef, useState} from "react";
import Slider from "react-slick";
import {useTranslation} from "react-i18next";
import {fetchTestimonials} from "../../lib/testimonialFetch";
import {getCurrentLanguage} from "../../lib/langUtils";
import {CourseContext} from "../../context/CourseContext";

const TestimonialSlide = ({index, testimonial}) => (
    <div className="row" key={index}>
        <div className="testimonial testimonial1 icon-salon_quote col-lg-12 col-md-12 col-sm-12">
            <div className="pic pic2">
                <img src={testimonial.picture}/>
            </div>
            <div className="description">
                <span className="client-name">{testimonial.name}</span>
                <p>{testimonial.message}</p>
            </div>
        </div>
    </div>
)

const TestimonialsTitle = () => {
    const {t} = useTranslation();
    return (
        <div>
            <div className="container">
                <div className="wm-fancy-title wow zoomIn animated">
                    <img src="img/lotus.png"/>
                    <h2>{t('Testimonials')}</h2>
                </div>
            </div>
        </div>
    )
}

/**
 * Testimonials section on the front page.
 * @constructor
 */
export const Testimonials = () => {

    const [slides, setSlides] = useState([])
    const {i18n} = useTranslation();
    const getLanguage = () => getCurrentLanguage(i18n)
    const {courseLanguageData} = useContext(CourseContext)
    const slider = useRef(null)
    const {t} = useTranslation();

    useEffect(() => {
        console.log('courseLanguageData?.currentLanguage', courseLanguageData?.currentLanguage)
        async function fetchData() {
            let testimonials = await fetchTestimonials(getLanguage());
            shuffle(testimonials)
            setSlides(testimonials)
        }
        fetchData(); // No await
    }, [courseLanguageData?.currentLanguage])


    const shuffle = (a) => {
        for (let i = 0; i < a.length; i++) {
            const j = Math.floor(Math.random() * (i + 1))
            if (j !== i) {
                const tmp = a[i]
                a[i] = a[j]
                a[j] = tmp
            }
        }
    }

    const settings = {
        dots: false,
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 2,
        autoplay: false
    };

    const mobileSettings = {
        ...settings,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    const drawSlides = (slides) => {
        return slides.map((testimonial, index) => {
            return <TestimonialSlide key={index} index={index} testimonial={testimonial} />
        })
    }

    const renderSwipeWarning = () => (
        <span className="form-text text-muted swipe-message">{t("Swipe to see more results")}</span>
    )

    return (
        <>
            <TestimonialsTitle />
            <section id="testimonials" data-panel="testimonials"
                     className="col-12 testimonials testimonials-left testi d-none d-lg-block">
                <article>
                    <Slider {...settings} ref={slider}>
                        {drawSlides(slides)}
                    </Slider>
                    {renderSwipeWarning()}
                </article>
            </section>
            <section id="testimonials" data-panel="testimonials"
                     className="col-12 testimonials testimonials-left testi d-lg-none">
                <article>
                    <Slider {...mobileSettings} ref={slider}>
                        {drawSlides(slides)}
                    </Slider>
                    {renderSwipeWarning()}
                </article>
            </section>
        </>
    )
}