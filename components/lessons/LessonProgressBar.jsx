import React, {useContext} from 'react';
import {CourseContext} from "../../context/CourseContext";
import {Progress} from "reactstrap";

/**
 * Displays the progress in a lesson.
 * @returns {JSX.Element}
 * @constructor
 */
const LessonProgressBar = ({slider, classes = 'd-none d-lg-block'}) => {

    const {
        courseLanguageData
    } = useContext(CourseContext)

    const currentSlide = courseLanguageData?.currentSlide ? courseLanguageData?.currentSlide + 1 : 0
    const totalSlides = courseLanguageData?.currentSlide ? courseLanguageData?.sectionModules.length : 1
    const percentage = currentSlide / totalSlides * 100;
    const precentageDisplay = percentage.toFixed(2)

    return (
        <div className={`col-md-12 lesson-progress ${classes}`} onMouseDown={(e) => {
            const pageX = e.pageX;
            const boundingClientRect = e.currentTarget.getBoundingClientRect();
            const boundingClientRectWidth = boundingClientRect.width
            const boundingClientRectX = boundingClientRect.x
            const progressPercentage = (pageX - boundingClientRectX) / boundingClientRectWidth * 100
            const unitPercentage = (1.0 / courseLanguageData?.sectionModules.length) * 100
            const targetPage = Math.floor(progressPercentage / unitPercentage)
            console.log('Should move to ', targetPage)
            slider.current.slickGoTo(targetPage)
        }}>
            <Progress striped value={percentage}>{precentageDisplay}%</Progress>
        </div>
    )
}

export default LessonProgressBar