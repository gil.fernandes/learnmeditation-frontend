import React from 'react'
import {isPage, TYPE_PAGE} from "../../lib/moduleConstants";
import marked from "marked";

/**
 * Single lesson slide with some multimedia element a module title and the content html.
 * @param multimedia The multimedia element which might be a video, image or audio or a combination of these.
 * @param moduleTitle The module title, just text.
 * @param moduleContent Some HTML with mostly formatted text.
 * @param moduleType The type of module to be displayed
 * @param quizz The Quizz component.
 * @returns {JSX.Element}
 * @constructor
 */
const LessonSlide = ({
                         multimedia = (<div/>),
                         moduleTitle = "empty",
                         moduleContent = ("<div/>"),
                         moduleType = TYPE_PAGE,
                         quiz = (<div/>)
                     }) => {
    return (
        <div className="row slide-pr">
            <div className="col-lg-6 des d-none d-lg-block">
                {multimedia}
            </div>
            <div className="col-lg-6 scroll-lsn d-none d-lg-block">
                <div className="lsn-heading2">
                    <h4>{moduleTitle}</h4>
                </div>
                {isPage(moduleType) ? <div dangerouslySetInnerHTML={{__html: marked(moduleContent)}}/> : quiz}
            </div>
        </div>
    )
}

export default LessonSlide;