import {useContext, useState} from "react";
import {CourseContext, SET_FORM_SUBMISSION_START, SET_FORM_SUBMISSION_STOP} from "../../../context/CourseContext";
import mailSend from "../../../lib/mailSend";
import {useTranslation} from "react-i18next";

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

const isMailValid = (str) => str.length > 3 && validateEmail(str)

const buildSubmissionData = (quizAnswers, courseLanguageData, moduleTitle) => {
    const entries = Object.entries(quizAnswers);
    const length = entries.length;
    const submissionData = {
        "email": courseLanguageData.userMail,
        "title": moduleTitle,
        "securityCode": "11191234123123123131312312213231"
    }
    const halfLength = length / 2
    for (let i = halfLength; i < length; i++) {
        const entry = entries[i]
        submissionData[`${i - (halfLength - 1)}. ${entry[0]}`] = entry[1]
    }
    return submissionData;
};

/**
 * Used to submit a quiz to the server and emailing it.
 * @constructor
 */
const QuizSubmit = ({moduleTitle, quizAnswers}) => {

    const {t} = useTranslation();

    const {
        courseLanguageData, dispatch
    } = useContext(CourseContext)

    const [replyStatus, setReplyStatus] = useState(null);

    const emailValid = isMailValid(courseLanguageData.userMail)

    let statusTimeout = null

    const processFormSubmission = async (e) => {
        e.preventDefault()
        dispatch({type: SET_FORM_SUBMISSION_START})
        const submissionData = buildSubmissionData(quizAnswers, courseLanguageData, `${t('Learn Meditation Online')}: ${moduleTitle}`);
        console.log('submissionData', submissionData)
        const reply = await mailSend(submissionData)
        console.log('reply', reply)
        setReplyStatus(reply.status)
        dispatch({type: SET_FORM_SUBMISSION_STOP})
        if (statusTimeout) {
            clearTimeout(statusTimeout)
        }
        statusTimeout = setTimeout(() => setReplyStatus(null), 5000)
    }

    return (
        <li className="full btn-st">
            <div className="col-md-12">
                {courseLanguageData.submitting && <p>{t('Submitting form, please wait ...')}</p>}
                {replyStatus === 200 && <p className="alert-success">{t('Submission successful')}</p>}
                {replyStatus > 200 && <p className="alert-warning">{t('Submission failed')}</p>}
                <input type="submit" className="quizSubmit" value={t("Send responses to my email")} tabIndex="0"
                       disabled={!emailValid && !courseLanguageData.submitting} onClick={processFormSubmission}/>
            </div>
        </li>
    )
}

export default QuizSubmit