import React, {useContext} from 'react';
import {useTranslation} from "react-i18next";
import {preventBubble} from "../../../lib/eventsUtils";
import {CourseContext, SET_USER_MAIL} from "../../../context/CourseContext";

/**
 * Displays the email address field.
 * @constructor
 */
const QuizEmail = ({module}) => {
    const {
        courseLanguageData, dispatch
    } = useContext(CourseContext)

    const {t} = useTranslation();
    const id = `quizz_email_${module.id}`
    return (
        <li>
            <div className="col-md-12">
                <label htmlFor={id}>{t('My Email')} <a href={`#explanation_${module.id}`}>*</a></label>
                <input type="email" id={id} onMouseDown={preventBubble} value={courseLanguageData.userMail}
                       onChange={(e) => dispatch({type: SET_USER_MAIL, userMail: e.target.value})}/>
            </div>
        </li>
    )
}

export default QuizEmail;