import React, {useState, useEffect} from 'react';
import QuizEmail from "./QuizEmail";
import {useTranslation} from "react-i18next";
import {preventBubble} from "../../../lib/eventsUtils";
import QuizSubmit from "./QuizSubmit";

const renderQuestion = (q, i, quizAnswers, setQuizAnswers) => {
    switch (q.qtype) {
        case "essay":
            return <textarea name={q.name} id={`q${i}`} onMouseDown={preventBubble}
                             onChange={e => setQuizAnswers({...quizAnswers, [q.name]: e.target.value})}>{quizAnswers[q.name]}</textarea>
        default:
            return <img src="/img/lotus.png" alt={q.name}/>
    }
}

/**
 * Used to display a Quiz.
 * @param module
 * @returns {JSX.Element}
 * @constructor
 */
const Quiz = ({module}) => {

    const [quizAnswers, setQuizAnswers] = useState({})

    useEffect(() => {
        setQuizAnswers(module.questions.map((q) => {
            return {
                [q.name]: ""
            }
        }))
    }, [])

    const {t} = useTranslation();
    return <form>
        <ul>
            {module.questions.map((q, i) => {
                return <li key={i}>
                    <div className="col-md-12">
                        <label className="pb-20 pb-rs" htmlFor={`q${i}`}>{q.name}</label>
                        {renderQuestion(q, i, quizAnswers, setQuizAnswers)}
                    </div>
                </li>
            })}
            <QuizEmail module={module}/>
            <QuizSubmit quizAnswers={quizAnswers} moduleTitle={module.quizName}/>
            <div className="col-md-12">
                <hr />
                <a name={`explanation_${module.id}`}><p className="small-p"><small>{t('my-email-explanation')}</small></p></a>
            </div>
        </ul>
    </form>

}

export default Quiz