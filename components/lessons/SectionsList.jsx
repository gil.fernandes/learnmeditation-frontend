import React, {useContext, useEffect} from 'react';
import {useTranslation} from "react-i18next";
import {getCurrentLanguage} from "../../lib/langUtils";
import {
    CourseContext,
    SET_CURRENT_SECTION_INDEX,
    SET_LANGUAGE,
    SET_SECTION_INFO,
    START_LOADING_LESSON
} from "../../context/CourseContext";
import {querySectionsOnly} from "../../lib/graphQLClient";


/**
 * Contains the list of sections in a course which you can click to navigate through the course.
 * @constructor
 */
const SectionsList = () => {

    const {
        courseLanguageData, dispatch
    } = useContext(CourseContext)

    const {i18n, t} = useTranslation();

    useEffect(() => {
        const setCourseLang = async () => {
            const currentLanguage = getCurrentLanguage(i18n)
            dispatch({type: SET_LANGUAGE, currentLanguage})
        }
        setCourseLang().then(() => {
            const courseId = courseLanguageData.selectedCourseId;
            return querySectionsOnly(courseId)
        }).then((sectionsInfo) => {
            dispatch({type: SET_SECTION_INFO, sectionsInfo})
            console.log('Course with sections', sectionsInfo)
        })
    }, [])

    const activateSection = async (e, id, index) => {
        e.preventDefault()
        if (!isActive(index)) {
            if (!courseLanguageData.loadingLesson) {
                dispatch({type: SET_CURRENT_SECTION_INDEX, currentSectionIndex: index})
            }
        }
    }

    const sections = courseLanguageData?.sectionsInfo.sections || []

    const isActive = (i) => courseLanguageData?.currentSectionIndex === i



    return (
        <div className="container lessons-titles">
            <div className="row">
                <div className="col-md-12 text-center">
                    <ul className="nav nav-tabs nv-tbs d-flex d-inline-flex lessons" role="tablist">
                        {
                            sections.map((section, i) => {
                                return (
                                    <li key={i} role="presentation"
                                        className={`${i === sections.length - 1 ? "brd-n" : ""} ${isActive(i) && "active" || ""}`}>
                                        <a href={`#lesson${i}`} aria-controls={`lesson${i}`} role="tab"
                                           data-toggle="tab"
                                           className={`lesson${i}}`}
                                           title={section.name}
                                           onClick={(e) => activateSection(e, section.id, i)}
                                        >
                                            {t('Lesson')} {i + 1}
                                        </a>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
            </div>
        </div>
    )
};

export default SectionsList