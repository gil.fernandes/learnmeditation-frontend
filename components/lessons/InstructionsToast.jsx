import {Toast, ToastBody, ToastHeader} from "reactstrap";
import {useTranslation} from "react-i18next";
import {CourseContext} from "../../context/CourseContext";
import {useContext, useState, useEffect} from "react";
import {findCourseForLang} from "../../lib/langConfig";

const SHOW_INSTRUCTIONS = 'show-instructions-toast';
/**
 * Toast with Instructions about the usage.
 * @returns {JSX.Element}
 * @constructor
 */
const InstructionsToast = () => {

    const {initialLanguage} = useContext(CourseContext)
    const {t} = useTranslation();

    const [show, setShow] = useState(true);

    const toggle = () => {
        setShow(!show)
        localStorage.setItem(SHOW_INSTRUCTIONS, false)
    };

    useEffect(() => {
        const showInstructions = localStorage.getItem(SHOW_INSTRUCTIONS);
        if (showInstructions === "false") {
            setShow(false)
        }
    })

    return (
        <div style={{ position: "fixed", bottom: "10px", right: "10px", zIndex: 9999 }}>
            <Toast isOpen={show}>
                <ToastHeader icon="info" toggle={toggle}>
                    {t("Usage Instructions")}
                </ToastHeader>
                <ToastBody dangerouslySetInnerHTML={
                    {__html: t('usage-intruction-lessons',
                            {interpolation: {escapeValue: false}, 'audio_link': `/meditateNow/${findCourseForLang(initialLanguage)}`,
                                'download_link': `/download/audio/${findCourseForLang(initialLanguage)}`})}
                }>
                </ToastBody>
            </Toast>
        </div>
    )
}

export default InstructionsToast;