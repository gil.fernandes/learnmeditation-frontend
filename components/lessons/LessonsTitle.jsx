import React from 'react';
import {useTranslation} from "react-i18next";


/**
 * Contains the Lessons static title.
 * @constructor
 */
const LessonsTitle = () => {

    const {t} = useTranslation();

    return (
        <div className="wm-main-content">
            <div className="wm-simple-fancy-title wm-simple-fancy-title1">
                <img src="/img/lotus.png" className="svg1" />
                <h2>{t('Our Lessons')}</h2>
            </div>
        </div>
    )
}

export default LessonsTitle