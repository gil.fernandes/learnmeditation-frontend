import React, {useContext, useEffect, useRef} from 'react';
import {CourseContext, SET_CURRENT_SECTION_INDEX, SET_CURRENT_SLIDE} from "../../context/CourseContext";
import {isPage, isQuiz} from "../../lib/moduleConstants";
import LessonSlide from "./LessonSlide";
import PageInfo from "./PageInfo";
import Slider from "react-slick";
import {LeftNavButton} from "../slider/LeftNavButton";
import {RightNavButton} from "../slider/RightNavButton";
import LessonProgressBar from "./LessonProgressBar";
import AudioPlayerReact from "../audio/AudioPlayerReact";
import {targetServer} from "../../lib/apiConstants";
import {VideoPlayer} from "./video/VideoPlayer";
import Quiz from "./quiz/Quiz";
import MobileLessonSlide from "./MobileLessonSlide";

const sliderSettings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false
};

const extractTitle = (moduleInfo, module) => {
    if (isPage(moduleInfo.courseModuleType)) {
        return module.pageName;
    } else if (isQuiz(moduleInfo.courseModuleType)) {
        return module.quizName;
    }
};

const extractContent = (moduleInfo, module) => {
    if (isPage(moduleInfo.courseModuleType)) {
        return module.pageContent;
    } else if (isQuiz(moduleInfo.courseModuleType)) {
        return "";
    }
};

const extractQuizz = (moduleInfo, module) => {
    if (isQuiz(moduleInfo.courseModuleType)) {
        return <Quiz module={module}/>
    } else {
        return <div/>
    }
};

/**
 * Main Lesson slider which supports mobile and non mobile views.
 * @constructor
 */
export const LessonSlider = ({sections}) => {

    const {
        courseLanguageData, dispatch
    } = useContext(CourseContext);

    const slider = useRef(null)

    const mobileSlider = useRef(null)

    const sectionsAvailable = () => sections.length > 0

    useEffect(() => {

        async function setModule() {
            if (sectionsAvailable()) {
                [slider, mobileSlider]
                    .forEach(s => s?.current?.slickGoTo(courseLanguageData.showLast ? courseLanguageData.sectionData.modules.length - 1 : 0))
            }
        }

        setModule();
    }, [courseLanguageData.currentSectionIndex]);

    const displayMultimedia = currentModule => {
        const modules = currentModule.modules
        const defaultImage = '/uploads/Lesson_Overview_Artwork_01_9008d3e257.png';
        let alt = currentModule.pageName || currentModule.quizzName
        if (modules?.length > 0) {
            const module = modules[0] || {};
            const image = (module.image || {}).url || defaultImage;
            const audio = module.audio
            const video = module.video
            return (
                <div className="container">
                    <div className="row audio-row">
                        <div className="col-12">
                            {audio && <AudioPlayerReact audio={audio}/>}
                            {!audio && !video && <div style={{height: "60px"}}/>}
                        </div>
                    </div>
                    {image && <div className="row">
                        <div className="col-12">
                            <img src={`${targetServer}${image}`} alt={alt} title={alt} className="img-fluid"/>
                        </div>
                    </div>}
                    {video && <div className="row video-row">
                        <div className="col-12">
                            <VideoPlayer video={video}/>
                        </div>
                    </div>}
                </div>
            )

        }
        return <img src={`${targetServer}${defaultImage}`} alt={alt} title={alt} className="img-fluid"/>
    };

    const drawSlides = (drawFunc) => {
        if (courseLanguageData.sectionData && courseLanguageData.moduleData) {
            return courseLanguageData.moduleData
                .map((module, key) => {
                    return {moduleInfo: courseLanguageData.sectionModules[key], key, module}
                })
                .filter(({moduleInfo, key}) => {
                    console.log('moduleInfo', moduleInfo)
                    return moduleInfo.accessible
                }).map(({moduleInfo, key, module}) => {
                    const title = extractTitle(moduleInfo, module)
                    const moduleContent = extractContent(moduleInfo, module)
                    const quiz = extractQuizz(moduleInfo, module)
                    return drawFunc(key, module, title, moduleContent, moduleInfo, quiz)
                })
        }
        return <></>
    }

    const drawDesktopSlides = () => {
        return drawSlides((key, module, title, moduleContent, moduleInfo, quiz) => {
            return (
                <div key={key}>
                    <div className="widget_slider_wrap widget_slider_wrap1">
                        <LessonSlide multimedia={displayMultimedia(module)} moduleTitle={title}
                                     moduleContent={moduleContent}
                                     moduleType={moduleInfo.courseModuleType}
                                     quiz={quiz}/>
                        <MobileLessonSlide multimedia={displayMultimedia(module)} moduleTitle={title}
                                           moduleContent={moduleContent}
                                           moduleType={moduleInfo.courseModuleType}
                                           quiz={quiz}/>
                    </div>
                    <PageInfo/>
                </div>
            )
        })
    }

    const isNotLastLesson = () => courseLanguageData?.currentSectionIndex < courseLanguageData?.sectionsInfo?.sections.length - 1;

    const isNotFirstLesson = () => courseLanguageData?.currentSectionIndex > 0;

    const isNotLastSlideInLesson = () => courseLanguageData?.moduleData && courseLanguageData.currentSlide < courseLanguageData?.moduleData.length - 1

    const isNotFirstSlideInLesson = () => courseLanguageData?.currentSlide > 0;

    const showLeftNav = () => isNotFirstLesson() || isNotFirstSlideInLesson();

    const showRightNav = () => isNotLastLesson() || isNotLastSlideInLesson();

    const navigationToNextLesson = () => {
        if (isNotLastLesson() && !isNotLastSlideInLesson()) {
            dispatch({type: SET_CURRENT_SECTION_INDEX, currentSectionIndex: courseLanguageData.currentSectionIndex + 1})
        }
    }

    const navigationToPreviousLesson = () => {
        if (isNotFirstLesson() && !isNotFirstSlideInLesson()) {
            const previousSectionIndex = courseLanguageData.currentSectionIndex - 1;
            dispatch({type: SET_CURRENT_SECTION_INDEX, currentSectionIndex: previousSectionIndex, showLast: true})
        }
    }

    return (
        <>
            <div className="slider-container">
                <Slider {...sliderSettings}
                        prevArrow={<LeftNavButton slider={slider} show={showLeftNav()}
                                                  callback={navigationToPreviousLesson}/>}
                        nextArrow={<RightNavButton slider={slider} show={showRightNav()}
                                                   callback={navigationToNextLesson}/>}
                        afterChange={current => {
                            dispatch({type: SET_CURRENT_SLIDE, currentSlide: current})
                        }}
                        ref={slider}>
                    {drawDesktopSlides()}
                </Slider>
            </div>
            <LessonProgressBar slider={slider}/>
        </>
    )
}

export default LessonSlider