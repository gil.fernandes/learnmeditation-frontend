import React, {useContext} from 'react';
import {CourseContext} from "../../context/CourseContext";
import {useTranslation} from "react-i18next";

/**
 * Displays the page and the lesson modules total.
 * @returns {JSX.Element}
 * @constructor
 */
const PageInfo = () => {

    const {
        courseLanguageData
    } = useContext(CourseContext)

    const {t} = useTranslation();

    return (
        <div className="col-md-12 page-n-container">
            <div className="page-n"><p>{t('Page')} {courseLanguageData?.currentSlide + 1} {t('of')} {' '}
                {courseLanguageData?.sectionModules ? courseLanguageData?.sectionModules.length : "--"}</p></div>
        </div>
    )
}

export default PageInfo