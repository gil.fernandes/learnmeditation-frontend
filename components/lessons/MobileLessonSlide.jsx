import React from 'react'
import {isPage, TYPE_PAGE} from "../../lib/moduleConstants";

/**
 * Single lesson slide with some multimedia element a module title and the content html.
 * @param multimedia The multimedia element which might be a video, image or audio or a combination of these.
 * @param moduleTitle The module title, just text.
 * @param moduleContent Some HTML with mostly formatted text.
 * @param moduleType The type of module to be displayed
 * @param quizz The Quizz component.
 * @returns {JSX.Element}
 * @constructor
 */
const MobileLessonSlide = ({
                         multimedia = (<div/>),
                         moduleTitle = "empty",
                         moduleContent = ("<div/>"),
                         moduleType = TYPE_PAGE,
                         quiz = (<div/>)
                     }) => {
    return (
        <div className="row slide-pr d-lg-none">
            <div className="col-md-12 scroll-lsn">
                <div className="lsn-heading2">
                    <h4>{moduleTitle}</h4>
                </div>
                {isPage(moduleType) ? <div dangerouslySetInnerHTML={{__html: moduleContent}}/> : quiz}
            </div>
            <div className="col-md-12 mobile-media">
                {multimedia}
            </div>
        </div>
    )
}

export default MobileLessonSlide;