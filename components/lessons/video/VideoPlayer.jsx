import {controls, Media, Player, utils} from "react-media-player";
import {targetServer} from "../../../lib/apiConstants";
import React from "react";
import VideoPlayPause from "./VideoPlayPause";
import CustomMuteUnmute from "../../audio/CustomMuteUnmute";
import CustomFullscreen from "./CustomFullscreen";

const {PlayPause, MuteUnmute, Progress, SeekBar} = controls

const {
    CurrentTime,
    Duration,
    Volume
} = controls
const {formatTime} = utils

export const VideoPlayer = ({video}) => {
    return (
        <Media>
            <div className="media" onMouseDown={(e) => e.stopPropagation()}>
                <div className="video-container">
                    <div className="media-player">
                        <Player src={`${targetServer}${video.url}`}/>
                    </div>
                    <div className="media-controls">
                        <VideoPlayPause classeName="media-control media-control--play-pause"/>
                        <div className="media-control-group media-control-group--seek">
                            <SeekBar className="media-control media-control--seekbar" />
                            <div className="media-time-mute-info">
                                <CurrentTime className="media-control media-control--current-time" />/<Duration className="media-control media-control--duration" />
                                <CustomMuteUnmute />
                            </div>
                            <Volume className="media-control media-control--volume" />
                            <CustomFullscreen className="media-control media-control--fullscreen"/>
                        </div>
                    </div>
                </div>
            </div>
        </Media>
    )

}