import React, {useContext} from 'react';
import {CourseContext} from "../../context/CourseContext";

/**
 * Component used to depict that something is loading.
 * @param loading
 * @returns {JSX.Element}
 * @constructor
 */
const Loading = () => {
    const {
        courseLanguageData
    } = useContext(CourseContext)

    return (
        courseLanguageData.loadingLesson && <div className="preloader-wrapper-custom">
            <div className="preloader">
                <img src="/img/loader.gif" alt="" />
            </div>
        </div>
    )
}

export default Loading