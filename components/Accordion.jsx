import {useState} from "react";

/**
 * Contains bare-bone functionality for all accordion style components.
 * @param items The items to be rendered
 * @param renderItems The actual function which renders the items.
 * @constructor
 */
const Accordion = ({items, renderItems}) => {

    const [expandedItem, setExpandedItem] = useState(0)

    const toggle = (i) => {
        if(i === expandedItem) {
            setExpandedItem(-1)
        } else {
            setExpandedItem(i)
        }
    }

    return renderItems(items, expandedItem, toggle)
}

export default Accordion