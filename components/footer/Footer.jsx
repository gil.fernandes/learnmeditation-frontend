import SocialIcons from "../SocialIcons";
import React, {useState, useEffect, useContext} from "react";
import {AboutUs, Contact, Help, HowTo, Legal} from "../menus/MenuItems";
import {useTranslation} from "react-i18next";
import {CourseContext} from "../../context/CourseContext";
import {getCurrentLanguage} from "../../lib/langUtils";
import {LANGUAGES_WITH_HELP, LANGUAGES_WITH_LEGAL} from "../../lib/langConfig";

/**
 * Footer to be used on all pages.
 * @returns {JSX.Element}
 * @constructor
 */
const Footer = () => {
    const {initialLanguage, setInitialLanguage} = useContext(CourseContext)
    const {i18n, t} = useTranslation();

    useEffect(() => {
        setInitialLanguage(getCurrentLanguage(i18n))
    }, [])


    const hasHelp = LANGUAGES_WITH_HELP.includes(initialLanguage)
    const hasLegal = LANGUAGES_WITH_LEGAL.includes(initialLanguage)
    console.log('initialLanguage footer', initialLanguage, hasHelp)

    return (
        <footer className="site-footer">
            <div className="lower-footer">
                <div className="container">
                    <div className="row">
                        <div className="col col-xs-12">
                            <p className="copyright"><AboutUs useLi={false}/> | <HowTo useLi={false}/> {hasHelp && <>| <Help useLi={false} /></>} | <Contact useLi={false}/> {hasLegal && <> | <Legal useLi={false}/></>} | © Copyright {new Date().getFullYear()} | {t('Designed by')} <a
                                href="https://www.excitetemplate.com/" target="_blank">Excite Systems</a></p>
                            <div className="extra-link demopadding">
                                <SocialIcons />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer