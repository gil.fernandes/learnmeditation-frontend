import React, {useState} from 'react';
import {Form, FormGroup, Input, Label} from "reactstrap";
import mailSend from "../lib/mailSend";

const formValid = (mail) => {
    return !!mail && /^[a-zA-Z0-9.]+@(?:[a-zA-Z0-9]+\.)+[A-Za-z]+$/.test(mail);
};

const renderMailField = (mail, setMail) => renderField('Email', mail, setMail);

const renderSecurityCode = (securityCode, setSecurityCode) => renderField('Security code', securityCode, setSecurityCode);

const renderField = (fieldLabel, value, setValue) => {
    return (
        <div className="col-md-12 module-content">
            <FormGroup>
                <Label for="Email">{fieldLabel} *</Label>
                <Input type="email" name="text" id="Email" value={value} onChange={(e) => setValue(e.target.value)}/>
            </FormGroup>
        </div>
    );
};

async function sendMail (e, mail, qizz, securityCode, quizzContent) {
    console.log('quizzContent', quizzContent);
    e.preventDefault();
    let quizzData = quizzContent.reduce((a, r, i) => {
        console.log('i', i);
        console.log('qizz.questions[i]', qizz.questions[i]);
        a[qizz.questions[i].name] = r;
        return a;
    }, {});
    quizzData['email'] = mail;
    quizzData['securityCode'] = securityCode;
    quizzData['title'] = qizz.quizName;
    console.log('quizzData', quizzData);
    const res = await mailSend(quizzData);
    console.log(res);
}

/**
 * The form used to display the quizzes.
 * @param quizz The quizz data
 * @return {*}
 * @constructor
 */
const QuizzForm = ({quizz}) => {
    const [mail, setMail] = useState("");
    const [securityCode, setSecurityCode] = useState("");
    const [quizzContent, setQuizzContent] = useState([]);

    return (
        <Form>
            {quizz.questions.map((q, i) => {
                return (
                    <div className="col-md-12 module-content" key={i}>
                        <FormGroup>
                            <Label for={q.id.toString()}>{q.name}</Label>
                            {q.qtype === 'essay' ? (
                                <Input type="textarea" name="text" id={q.id} onChange={(e) => {
                                    quizzContent[i] = e.target.value;
                                    setQuizzContent([...quizzContent]);
                                }}/>
                            ) : ''}
                        </FormGroup>
                    </div>
                )
            })}
            {renderMailField(mail, setMail)}
            {renderSecurityCode(securityCode, setSecurityCode)}
            <div className="col-md-12 module-content">
                <button className="btn btn-primary" disabled={!formValid(mail)}
                        onClick={(e) => sendMail(e, mail, quizz, securityCode, quizzContent)}>Send to yourself</button>
            </div>
        </Form>
    )
};

export default QuizzForm;