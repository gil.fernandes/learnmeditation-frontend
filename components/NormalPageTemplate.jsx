import React from 'react';
import {CourseContext} from "../context/CourseContext";
import {PageHeader} from "./PageHeader";
import {withTranslation} from "react-i18next";
import MenuHeader from "./menus/MenuHeader";
import {MenuLogo} from "./menus/MenuItems";
import {setupMenuScrollBehaviour} from "../lib/menuUtils";
import ImageBanner from "./banner/ImageBanner";
import Footer from "./footer/Footer";
import '../lib/i18n';

/**
 * The actual layout for the lessons page
 */
class NormalPageTemplate extends React.Component {

    static contextType = CourseContext;

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        setupMenuScrollBehaviour()
    }

    static async getInitialProps({}) {
        let pageProps = {};
        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }

        return {pageProps};
    }

    render() {
        const {t} = this.props;
        const title = t("Learn Meditation Online");
        const description = t("Meta description");
        return (
            <>
                <PageHeader description={description} title={title}/>
                <MenuHeader extraMenuClasses={["menuLessons"]} menoLogo={<MenuLogo image="/img/logo2.png"/>}/>
                <ImageBanner bannerStyle={this.props.bannerStyle} titleKey={this.props.titleKey}/>
                {this.props.children}
                <Footer/>
            </>
        )
    }
}

export default withTranslation()(NormalPageTemplate);