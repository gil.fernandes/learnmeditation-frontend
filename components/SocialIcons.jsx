import {useTranslation} from "react-i18next";
import {useContext, useEffect, useState} from "react";
import {CourseContext} from "../context/CourseContext";
import {getCurrentLanguage} from "../lib/langUtils";

export const defaultWebsite = 'https://learnmeditationonline.org'

const targetWebsites = {
    "en": defaultWebsite,
    "de": "https://learnmeditationonline.org?lng=de"
}

const chooseTargetWebsite = (lang) => {
    if(targetWebsites[lang]) {
        return targetWebsites[lang]
    }
    return defaultWebsite
}

/**
 * Social icons to be displayed on page for sharing of the webpage.
 * @returns {JSX.Element}
 * @constructor
 */
export const SocialIcons = () => {

    const {courseLanguageData} = useContext(CourseContext)
    const [website, setWebsite] = useState(defaultWebsite)
    const {i18n, t} = useTranslation();

    useEffect(() => {
        const lang = getCurrentLanguage(i18n);
        console.log('Social icons language', lang)
        setWebsite(chooseTargetWebsite(lang))
    }, [courseLanguageData])

    const encodedTargetWebsite = encodeURI(website)

    return (
        <ul>
            <li>
                <a href={`https://www.facebook.com/sharer/sharer.php?u=${encodedTargetWebsite}`} target="_blank" className="icon social fb">
                    <i className="fa fa-facebook" />
                </a>
            </li>
            <li>
                <a href={`http://twitter.com/share?text=${encodeURI((t('Learn Meditation Online')))}&url=${encodedTargetWebsite}&hashtags=${encodeURI(t('twitter-hashtags'))}`} target="_blank">
                    <i className="fa fa-twitter" />
                </a>
            </li>
            <li>
                <a href={`https://www.linkedin.com/sharing/share-offsite/?url=${encodedTargetWebsite}`} target="_blank">
                    <i className="fa fa-linkedin" />
                </a>
            </li>
        </ul>
    )
}

export default SocialIcons