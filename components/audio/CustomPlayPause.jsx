import React, {Component} from 'react'
import {withMediaProps} from 'react-media-player'

/**
 * Customized play pause button.
 */
class CustomPlayPause extends Component {
    shouldComponentUpdate({media}) {
        return this.props.media.isPlaying !== media.isPlaying
    }

    _handlePlayPause = (e) => {
        e.preventDefault()
        this.props.media.playPause()
    }

    render() {
        const {className, style, media} = this.props
        return (
            media && <div className="play-button-container">
                <span className="play-button">
                    {media.isPlaying ?
                        <a href="#" onClick={this._handlePlayPause}><i className="fa fa-pause"/></a> :
                        <a href="#" onClick={this._handlePlayPause}><i className="fa fa-play"/></a>}
                </span>
            </div>
        )
    }
}

export default withMediaProps(CustomPlayPause)