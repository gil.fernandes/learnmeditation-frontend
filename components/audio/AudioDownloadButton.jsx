import {targetServer} from "../../lib/apiConstants";
import {useTranslation} from "react-i18next";

/**
 * Button used to download the audio file.
 * @constructor
 */
const AudioDownloadButton = ({audio}) => {

    const {t} = useTranslation();

    return (
        <div className="wm-custom-icons wm-custom-icons1">
            <a href={`${targetServer}${audio.url}`} target="_blank" className="wm-download" title={t('Download')} download={true}
               tabIndex="0"><i className="fi-interface-download" /></a>
        </div>
    )
}

export default AudioDownloadButton