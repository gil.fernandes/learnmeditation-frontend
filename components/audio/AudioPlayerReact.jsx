import React from 'react';
import {controls, Media, Player, utils} from 'react-media-player'
import {targetServer} from "../../lib/apiConstants";
import CustomPlayPause from "./CustomPlayPause";
import CustomMuteUnmute from "./CustomMuteUnmute";
import AudioDownloadButton from "./AudioDownloadButton";

const {PlayPause, MuteUnmute, Progress, SeekBar} = controls

const {
    CurrentTime,
    Duration,
    Volume,
} = controls
const {formatTime} = utils

const AudioPlayerReact = ({audio}) => {
    return (
        audio?.url && <Media>
            <div className="media" onMouseDown={(e) => e.stopPropagation()}>
                <div className="media-player">
                    <Player src={`${targetServer}${audio.url}`} useAudioObject/>
                </div>
                <div className="media-controls">
                    <CustomPlayPause/>
                    <SeekBar className="media-control media-control--play"/>
                    <CurrentTime className="media-control media-control--current-time"/>
                    <div className="media-control--current-time">/</div>
                    <Duration className="media-control media-control--duration"/>
                    <CustomMuteUnmute/>
                    <Volume className="media-control media-control--volume"/>
                    <AudioDownloadButton audio={audio}/>
                </div>
            </div>
        </Media> || <></>
    )
}

export default AudioPlayerReact