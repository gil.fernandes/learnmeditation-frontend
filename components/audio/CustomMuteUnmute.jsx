import React, {Component} from 'react'
import {withMediaProps} from 'react-media-player'

/**
 * Customized play pause button.
 */
class CustomMuteUnmute extends Component {

    _handleMuteUnmute = (e) => {
        e.preventDefault()
        this.props.media.muteUnmute()
    }

    render() {
        const { volume, media } = this.props
        return (
            media && <div className="mute-button-container">
                <span className="mute-button">
                    {!media.isMuted ?
                        <a href="#" onClick={this._handleMuteUnmute}><i className="fa fa-volume-up" /></a> :
                        <a href="#" onClick={this._handleMuteUnmute}><i className="fa fa-volume-off" /></a>}
                </span>
            </div>
        )
    }
}

export default withMediaProps(CustomMuteUnmute)