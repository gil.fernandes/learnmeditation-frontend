import React, {useContext, useState, useEffect} from 'react';
import {CourseContext} from "../../context/CourseContext";
import {targetServer} from "../../lib/apiConstants";

const TimeDisplay = ({currentTime, duration}) => {

    const zeroPad = (num, places) => String(num).padStart(places, '0')

    const displayMinutes = (time) => zeroPad(Math.floor( time/ 60), 1)

    const displaySeconds = (time) => zeroPad(Math.floor(time % 60), 2)

    return (
        <span>{displayMinutes(currentTime)} : {displaySeconds(currentTime)} / {displayMinutes(duration)} : {displaySeconds(duration)}</span>
    )
}

/**
 * Simple audio player used to play audio files.
 * Deprecated!
 * @returns {JSX.Element}
 * @constructor
 */
export const AudioPlayer = ({audio}) => {

    const [playing, setPlaying] = useState(false)
    const [currentTime, setCurrentTime] = useState(0)

    function getAudioPlayer() {
        return document.getElementById(audio.id);
    }

    useEffect(() => {
        const audioPlayer = getAudioPlayer()
        audioPlayer.ontimeupdate = (event) => {
            setCurrentTime(audioPlayer.currentTime)
        };
    }, [])

    const play = (e) => {
        e.preventDefault()
        getAudioPlayer().play()
        setPlaying(true)
    }

    const pause = (e) => {
        e.preventDefault()
        getAudioPlayer().pause()
        setPlaying(false)
    }

    return (
        <>
            <audio
                controls
                src={`${targetServer}${audio.url}`}
                id={audio.id}
            >
                Your browser does not support the
                <code>audio</code> element.
            </audio>
            {!playing && <a href="#" onClick={play}><i className="fa fa-play"/></a>}
            {playing && <a href="#" onClick={pause}><i className="fa fa-pause" /></a>}
            {<TimeDisplay currentTime={currentTime} duration={(getAudioPlayer() || {}).duration}/> }
        </>
    )
}

export default AudioPlayer