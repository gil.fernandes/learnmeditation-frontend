/**
 * Left navigation button used to navigate back in the slides
 * @param slider
 * @param show
 * @param callback
 * @returns {*|JSX.Element}
 * @constructor
 */
export const LeftNavButton = ({slider, show=true, callback=() => {}}) => {
    return (
        show && <button className="slick-prev slick-prev-dark fa fa-step-backward"
                onClick={() => {
                    slider.current.slickPrev()
                    callback()
                }}/>
    )
}