import React from "react";

/**
 * Slider right navigation button.
 * @param slider The reference to the slider.
 * @param show If{@code true} then the button is displayed, else not.
 * @param callback The callback.
 * @returns {*|JSX.Element}
 * @constructor
 */
export const RightNavButton = ({slider, show=true, callback=() => {}}) => {
    return (
        show && <button className="slick-next slick-next-dark fa fa-step-forward"
                onClick={() => {
                    slider.current.slickNext()
                    callback()
                }}/>
    )
}