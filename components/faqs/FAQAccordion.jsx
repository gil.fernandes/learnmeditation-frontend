import React from "react";
import {Collapse} from 'reactstrap';
import Accordion from "../Accordion";

/**
 * Accordion for the FAQs based on the simple accordion.
 * @param items The items to render.
 * @constructor
 */
export const FAQAccordion = ({items}) => {

    const renderItems = (items, expandedItem, toggle) => (
        <div className="container-fluid faq-container">
            <div className="accordion">
                {items.map((fg, i) => {
                    const isExpanded = i === expandedItem
                    return (
                        <div className="card mb-0" key={i}>
                            <div className={`card-header ${!isExpanded && 'collapsed'}`} data-toggle="collapse" href="#collapseOne"
                                 aria-expanded={isExpanded} onClick={e => toggle(i)}>
                                <a className="card-title" onClick={e => toggle(i)} aria-expanded={isExpanded}>
                                    <i className="fa fa-question-circle" />{' '}{fg.Name}
                                </a>
                            </div>
                            <Collapse isOpen={isExpanded}>
                                <div className="faq-question-container">
                                    {fg.faqs.map((faq, i) => {
                                        return (
                                            <div className="faq-pg" key={i}>
                                                <h4 className="style-list"><i className="fa fa-question-circle" />{' '}{faq.Question}</h4>
                                                <div dangerouslySetInnerHTML={{__html: faq.Answer}}></div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </Collapse>
                        </div>
                    )
                })}
            </div>
        </div>
    )

    return <Accordion items={items} renderItems={renderItems}/>
}