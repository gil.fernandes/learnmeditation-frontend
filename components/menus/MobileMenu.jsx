import React, {useState, useContext, useEffect} from 'react';
import {
    Contact,
    DownloadAudio,
    DownloadPrint,
    DownloadVideo,
    GeneralFAQs,
    MenuLessons,
    MenuMeditateNow,
    UserFAQs
} from "../menus/MenuItems";
import {useTranslation} from "react-i18next";
import {findCourseForLang, findGeneralFAQCategoryForLang, findUserFAQCategoryForLang} from "../../lib/langConfig";
import Link from "next/link";
import {CourseContext} from "../../context/CourseContext";
import {getCurrentLanguage} from "../../lib/langUtils";

/**
 * Mobile menu for the website.
 * @returns {JSX.Element}
 * @constructor
 */
const MobileMenu = () => {

    const {initialLanguage, setInitialLanguage} = useContext(CourseContext)
    const {t, i18n} = useTranslation();
    const [menuOn, setMenuOn] = useState(false);
    const [menuDownloadOn, setMenuDownloadOn] = useState(false);
    const [menuFAQOn, setMenuFAQOn] = useState(false);

    useEffect(() => {
        setInitialLanguage(getCurrentLanguage(i18n))
        window.addEventListener('scroll', closeMenu);
        return () => {
            window.removeEventListener('scroll', closeMenu);
        }
    }, [])

    const toggleMenu = () => {
        setMenuFAQOn(false)
        setMenuDownloadOn(false)
        setMenuOn(!menuOn)
    }

    const closeMenu = () => {
        setMenuFAQOn(false)
        setMenuDownloadOn(false)
        setMenuOn(false)
    }

    const toggleMenuDownload = (e) => {
        e.preventDefault()
        setMenuDownloadOn(!menuDownloadOn)
        setMenuFAQOn(false)
    }

    const toggleMenuFAQOn = (e) => {
        e.preventDefault()
        setMenuDownloadOn(false)
        setMenuFAQOn(!menuFAQOn)
    }

    const printFolder = 'print'
    const audioFolder = 'audio'
    const videoFolder = 'video'

    const renderMenuItem = (folder, labelKey, path = 'download', ) => (
        <li onClick={closeMenu}>
            <Link href={`/${path}/${folder}/[id]`} as={`/${path}/${folder}/${findCourseForLang(initialLanguage)}`}>
                <a>{t(labelKey)}</a>
            </Link>
        </li>
    )

    return (
        <div id="mobilenav"><a id="mobilenav_trigger" className="icon-" onClick={toggleMenu}/>
            <nav className="menu-mobile">
                {menuOn && <ul>
                    <li className="highlight-menu highlight-menu1"><MenuMeditateNow/></li>
                    <li onClick={closeMenu}>
                        <Link href="/lessons/[id]" as={`/lessons/${findCourseForLang(initialLanguage)}`}>
                            <a className="nav-link">{t('Lessons')}</a>
                        </Link>
                    </li>
                    <li className="has-sub" onClick={toggleMenuDownload}><a>{t('Downloads')}</a>
                        {menuDownloadOn && <ul className="sub-menu dropdown-menu1">
                            {renderMenuItem(printFolder, "Print")}
                            {renderMenuItem(audioFolder, "Audio")}
                            {renderMenuItem(videoFolder, "Video")}
                        </ul>}
                        <span className="sub-trigger icon-"/><span className="sub-trigger icon-"/></li>
                    <li className="has-sub" onClick={toggleMenuFAQOn}><a>{t('FAQ')}</a>
                        {menuFAQOn && <ul className="sub-menu dropdown-menu1">
                            <li onClick={closeMenu}>
                                <Link href="/faqs/[id]" as={`/faqs/${findGeneralFAQCategoryForLang(initialLanguage)}`}>
                                    <a className="nav-link">{t('General FAQs')}</a>
                                </Link>
                            </li>
                            <li onClick={closeMenu}>
                                <Link href="/faqs/[id]" as={`/faqs/${findUserFAQCategoryForLang(initialLanguage)}`}>
                                    <a className="nav-link">{t('User FAQs')}</a>
                                </Link>
                            </li>
                        </ul>}
                        <span className="sub-trigger icon-"/><span className="sub-trigger icon-"/></li>
                    <li><Contact/></li>
                </ul>}
            </nav>
        </div>
    )
}

export default MobileMenu;