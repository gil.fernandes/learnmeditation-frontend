import React, {useContext, useEffect} from 'react';
import MobileMenu from "./MobileMenu";
import {useTranslation} from "react-i18next";
import {getCurrentLanguage} from "../../lib/langUtils";
import {
    Contact,
    DownloadAudio, DownloadPrint, DownloadVideo,
    GeneralFAQs,
    MenuLessons,
    MenuLogo,
    MenuMeditateNow, renderGeneralDownloadLink,
    renderGeneralFAQLink,
    UserFAQs
} from "./MenuItems";
import {CourseContext} from "../../context/CourseContext";
import SocialIcons from "../SocialIcons";
import {LANGUAGES_WITH_FAQS} from "../../lib/langConfig";

/**
 * Menu header.
 * @returns {JSX.Element}
 * @constructor
 */
const MenuHeader = ({extraMenuClasses = [], menoLogo = (<MenuLogo />)}) => {

    const {initialLanguage, setInitialLanguage} = useContext(CourseContext)
    const {i18n, t} = useTranslation();

    useEffect(() => {
        setInitialLanguage(getCurrentLanguage(i18n))
    }, [])

    return (
        <header className={("menuHeader " + extraMenuClasses.join(" ")).trim()}>
            <div className="container">
                <h1>
                    {menoLogo}
                </h1>
                <nav id="mainmenu" className="menu-horizontal nv-bar-nav">
                    <ul>
                        <MenuMeditateNow />
                        <MenuLessons />
                        <li>
                            {renderGeneralDownloadLink(initialLanguage, t, 'Download', '')}
                            <ul className="sub-menu dropdown-menu1">
                                <DownloadPrint />
                                <DownloadAudio />
                                <DownloadVideo />
                            </ul>
                        </li>
                        {LANGUAGES_WITH_FAQS.includes(initialLanguage) && <li>
                            {renderGeneralFAQLink(initialLanguage, t, 'FAQ', '')}
                            <ul className="sub-menu dropdown-menu1">
                                <GeneralFAQs/>
                                <UserFAQs/>
                            </ul>
                        </li>}
                        <Contact />
                    </ul>
                </nav>
                <div className="social-listm">
                    <div className="social-sub">
                        <nav className="animated">
                            <SocialIcons />
                        </nav>
                    </div>
                </div>
            </div>
            <MobileMenu />
        </header>
    )
}

export default MenuHeader;