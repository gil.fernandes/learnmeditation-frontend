import React, {useContext, useEffect} from 'react';
import {useTranslation} from "react-i18next";
import Link from "next/link";
import {findCourseForLang, findGeneralFAQCategoryForLang, findUserFAQCategoryForLang} from "../../lib/langConfig";
import {getCurrentLanguage} from "../../lib/langUtils";
import {CourseContext} from "../../context/CourseContext";

/**
 * The menu logo.
 * @returns {JSX.Element}
 * @constructor
 */
export const MenuLogo = ({image = "/img/logo.png"}) => {
    const {t} = useTranslation();
    return (
        <>
            <a href="/" title={t('Homepage')}>
                <img src={image} alt={t('Homepage')}/>
            </a>
        </>
    )
}

export const IdItemTemplate = ({renderElement}) => {
    const {initialLanguage, setInitialLanguage, courseLanguageData} = useContext(CourseContext)
    const {i18n, t} = useTranslation();

    useEffect(() => {
        setInitialLanguage(getCurrentLanguage(i18n))
    }, [courseLanguageData])

    return renderElement(initialLanguage, t)
}

/**
 * The meditate now menu item.
 * @returns {JSX.Element}
 * @constructor
 */
export const MenuMeditateNow = ({labelKey = 'Meditate now'}) => {

    const renderElement = (initialLanguage, t) => (
        <li className="highlight-menu highlight-menu1">
            <Link href="/meditateNow/[id]" as={`/meditateNow/${findCourseForLang(initialLanguage)}`}>
                <a href="#meditate-now" data-panel="meditate-now">{t(labelKey)}</a>
            </Link>
        </li>
    )

    return <IdItemTemplate renderElement={renderElement}/>
}

export const MenuLessons = () => {

    const renderElement = (initialLanguage, t) => (
        <li>
            <Link href="/lessons/[id]" as={`/lessons/${findCourseForLang(initialLanguage)}`}>
                <a className="nav-link">{t('Lessons')}</a>
            </Link>
        </li>
    )

    return <IdItemTemplate renderElement={renderElement}/>
}

export const renderGeneralFAQLink = (initialLanguage, t, labelKey = 'General FAQs', clazz = 'nav-link') => (
    <Link href="/faqs/[id]" as={`/faqs/${findGeneralFAQCategoryForLang(initialLanguage)}`}>
        <a className={clazz}>{t(labelKey)}</a>
    </Link>
)

export const renderGeneralDownloadLink = (initialLanguage, t, labelKey = 'Download', clazz = 'nav-link', folder = 'print') => (
    <Link href={`/download/${folder}/[id]`} as={`/download/${folder}/${findCourseForLang(initialLanguage)}`}>
        <a className={clazz}>{t(labelKey)}</a>
    </Link>
)

export const GeneralFAQs = () => {

    const renderElement = (initialLanguage, t) => (
        <li>
            {renderGeneralFAQLink(initialLanguage, t, 'General FAQs')}
        </li>
    )

    return <IdItemTemplate renderElement={renderElement}/>
}

const renderFaqListItem = (title, idFunc, initialLanguage) => {
    const {t} = useTranslation();
    return (
        <li>
            <Link href="/faqs/[id]" as={`/faqs/${idFunc(initialLanguage)}`}>
                <a className="nav-link">{t(title)}</a>
            </Link>
        </li>
    )
}

export const UserFAQs = () => {

    const renderElement = (initialLanguage, t) => (
        renderFaqListItem('User FAQs', findUserFAQCategoryForLang, initialLanguage)
    )

    return <IdItemTemplate renderElement={renderElement}/>
}

const renderDownloadElement = (initialLanguage, t, folder, titleKey) => (
    <li>
        <Link href={`/download/${folder}/[id]`} as={`/download/${folder}/${findCourseForLang(initialLanguage)}`}>
            <a>{t(titleKey)}</a>
        </Link>
    </li>
)

export const DownloadAudio = () => {

    const renderElement = (initialLanguage, t) => renderDownloadElement(initialLanguage, t, 'audio', 'Audio')

    return <IdItemTemplate renderElement={renderElement}/>
}

export const DownloadVideo = () => {

    const renderElement = (initialLanguage, t) => renderDownloadElement(initialLanguage, t, 'video', 'Video')

    return <IdItemTemplate renderElement={renderElement}/>
}

export const DownloadPrint = () => {

    const renderElement = (initialLanguage, t) => renderDownloadElement(initialLanguage, t, 'print', 'Print')

    return <IdItemTemplate renderElement={renderElement}/>
}

export const FlexibleMenuItem = ({useLi = true, path, messageKey}) => {

    const renderLink = (initialLanguage, t) => (
        <Link href={`/${path}/[id]`} as={`/${path}/${findCourseForLang(initialLanguage)}`}>
            <a>{t(messageKey)}</a>
        </Link>
    )

    const renderElement = (initialLanguage, t) => (
        <li>
            {renderLink(initialLanguage, t)}
        </li>
    )

    return <IdItemTemplate renderElement={useLi ? renderElement : renderLink}/>
}

export const Contact = ({useLi = true}) => {

    return <FlexibleMenuItem useLi={useLi} messageKey='Contact Us' path='contact' />
}

export const AboutUs = ({useLi = true}) => {

    return <FlexibleMenuItem useLi={useLi} messageKey='About Us' path='aboutUs' />
}

export const HowTo = ({useLi = true}) => {

    return <FlexibleMenuItem useLi={useLi} messageKey='How To' path='howto' />
}

export const Help = ({useLi = true}) => {

    return <FlexibleMenuItem useLi={useLi} messageKey='Help' path='help' />
}

export const Legal = ({useLi = true}) => {

    return <FlexibleMenuItem useLi={useLi} messageKey='Legal Information' path='legal' />
}