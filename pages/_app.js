/* _app.js */
import React from "react";
import App, {Container} from "next/app";
import Head from "next/head";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/global.css';
import '../styles/normalize.css';
import '../styles/animate.css';
import '../styles/color-three.css';
import '../styles/slick-slider.css';
import '../styles/plugins.css';
import '../styles/salon-font.css';

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import '../styles/exciteStyles.css';
import '../styles/exciteStyles2.css';
import '../styles/slick-extra.css';
import '../styles/audio.css';
import '../styles/video.css';
import '../styles/meditateNow.css';
import '../styles/faq.css';
import '../styles/download.css';
import '../styles/contact.css';
import '../styles/about-us.css';
import '../styles/howto.css';
import '../styles/help.css';
import '../styles/legal.css';
import 'flat-icons/interface.css'

import { withTranslation } from 'react-i18next';

import {CourseProvider} from '../context/CourseContext';

/**
 * Main entry point for the whole application.
 */
class MyApp extends App {

    static async getInitialProps({Component, router, ctx}) {
        let pageProps = {};

        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
        }
        return {pageProps};
    }

    render() {
        const {Component, pageProps} = this.props;
        return (
            <>
                <CourseProvider>
                    <Head/>
                    <Container>
                        <Component {...pageProps} />
                    </Container>
                </CourseProvider>
            </>
        );
    }
}

export default withTranslation()(MyApp);