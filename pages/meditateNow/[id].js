import React, {useEffect, useState} from 'react';
import {queryMeditations} from "../../lib/graphQLClient";
import {Nav, NavItem, NavLink, Row, TabContent, TabPane} from "reactstrap";
import NormalPageTemplate from "../../components/NormalPageTemplate";
import {useTranslation} from "react-i18next";
import classnames from "classnames";
import {MeditateNowAccordion} from "../../components/meditateNow/MeditateNowAccordion";

export default ({musicSections}) => {

    const [activeTab, setActiveTab] = useState(-1)
    const {t} = useTranslation();

    useEffect(() => {
        if (musicSections) {
            setActiveTab(musicSections[0].id)
        }
    }, [])

    console.log('musicSections', musicSections)

    const drawTabs = () => (
        <Nav tabs className="meditate-now-tabs">
            {musicSections.map((s, i) => {
                return (
                    <NavItem key={i}>
                        <NavLink
                            className={classnames({active: activeTab === s.id})}
                            onClick={() => {
                                setActiveTab(s.id)
                            }}
                        >
                            {s.name.toLowerCase()}
                        </NavLink>
                    </NavItem>
                );
            })}
        </Nav>
    )

    const drawTabContent = () => (
        <TabContent activeTab={activeTab} className="mt-3">
            {musicSections.map((s, i) => (
                <TabPane tabId={s.id} key={i}>
                    <MeditateNowAccordion items={s.audio_modules}/>
                </TabPane>
            ))}
        </TabContent>
    )

    return (
        <NormalPageTemplate titleKey={t("Meditate now")} bannerStyle="wm-mini-header-meditate-now">

            <div className="container meditate-now-container-main">
                <h4>{t('meditate-now-intro')}</h4>
                <Row>
                    {drawTabs()}
                </Row>
                <Row>
                    {drawTabContent()}
                </Row>
            </div>
        </NormalPageTemplate>
    )
}

export async function getServerSideProps({params}) {
    const musicSections = await queryMeditations(params.id);
    return {
        props: {
            musicSections
        }
    }
}