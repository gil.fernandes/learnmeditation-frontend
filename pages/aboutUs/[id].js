import React from "react";
import NormalPageTemplate from "../../components/NormalPageTemplate";
import {useTranslation} from "react-i18next";
import {queryAboutUsInfo} from "../../lib/graphQLClient";
import marked from "marked";
import {targetServer} from "../../lib/apiConstants";


/**
 * Displays the about us form.
 * @param musicSections
 * @returns {JSX.Element}
 */
export default ({aboutContent}) => {
    const {t} = useTranslation();

    console.log('aboutContent', aboutContent)

    return (
        <NormalPageTemplate titleKey={t("About Us")} bannerStyle="mini-header-about-us">
            <div className="container container-about-us">
                <div className="row">
                    <div className="col-md-7">
                        <div dangerouslySetInnerHTML={{__html: marked(aboutContent?.course.about_us)}}/>
                    </div>
                    <div className="col-md-5">
                        <img src={`${targetServer}${aboutContent?.course.about_us_image[0].url}`} className="img-fluid" />
                    </div>
                </div>
            </div>
        </NormalPageTemplate>
    )
}

export async function getServerSideProps({params}) {
    const aboutContent = await queryAboutUsInfo(params.id);
    return {
        props: {
            aboutContent
        }
    }
}