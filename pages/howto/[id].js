import React from "react";
import NormalPageTemplate from "../../components/NormalPageTemplate";
import {useTranslation} from "react-i18next";
import {queryAboutUsInfo, queryHowTo} from "../../lib/graphQLClient";
import marked from "marked";
import {targetServer} from "../../lib/apiConstants";


/**
 * Displays the how to page.
 * @param musicSections
 * @returns {JSX.Element}
 */
export default ({howtoContent}) => {
    const {t} = useTranslation();

    console.log('howtoContent', howtoContent)

    return (
        <NormalPageTemplate titleKey={t("How To")} bannerStyle="mini-header-how-to">
            <div className="container container-how-to">
                <div className="row">
                    <div className="col-md-7">
                        <div dangerouslySetInnerHTML={{__html: marked(howtoContent?.course.how_to)}}/>
                    </div>
                    <div className="col-md-5">
                        <img src={`${targetServer}${howtoContent?.course.image[0].url}`} className="img-fluid" />
                    </div>
                </div>
            </div>
        </NormalPageTemplate>
    )
}

export async function getServerSideProps({params}) {
    const howtoContent = await queryHowTo(params.id);
    return {
        props: {
            howtoContent
        }
    }
}