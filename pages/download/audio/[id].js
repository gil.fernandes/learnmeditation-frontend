import React from 'react';
import {queryMeditations} from "../../../lib/graphQLClient";
import {targetServer} from "../../../lib/apiConstants";
import DownloadDoubleLayout from "../../../components/download/DownloadDoubleLayout";

/**
 * Displays the audio download files.
 * @param musicSections
 * @returns {JSX.Element}
 */
export default ({musicSections}) => {

    const renderSingleItem = item => item.audio_modules.map((item, i) => (
        <li key={i}>
            <i className="fa fa-download"/>
            {' '} <a href={`${targetServer}${item.audio.url}`}
                     download={true}>{item.audio.caption || item.audio.name}</a>
        </li>
    ));

    return (
        <DownloadDoubleLayout titleKey="Audio" bannerStyle="mini-header-download-audio"
                              sections={musicSections}
                              renderSingleItem={renderSingleItem} />
    )
}

export async function getServerSideProps({params}) {
    const musicSections = await queryMeditations(params.id);
    return {
        props: {
            musicSections
        }
    }
}