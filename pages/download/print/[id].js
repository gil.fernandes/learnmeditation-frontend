import React, {useState} from "react";
import {queryPageTitles, queryVideos} from "../../../lib/graphQLClient";
import NormalPageTemplate from "../../../components/NormalPageTemplate";
import {useTranslation} from "react-i18next";
import DownloadAccordion from "../../../components/download/DownloadAccordion";
import {targetServer} from "../../../lib/apiConstants";
import DownloadDoubleLayout from "../../../components/download/DownloadDoubleLayout";


/**
 * Displays the video download files.
 * @param musicSections
 * @returns {JSX.Element}
 */
export default ({printSections}) => {

    const {t} = useTranslation();

    console.log('printSections', printSections)

    const filteredPrintSections = printSections.course.sections.filter(s => s.modules.filter(m => m.page).length > 0)

    function renderSingleItem(item) {
        return item?.modules?.filter(m => m.page && m.page.pageName).map((item, i) => (
            <li key={i}>
                <i className="fa fa-download"/>
                {' '} <a href={`${targetServer}/pdf/module?id=${item.id}`}
                         download={true}>{item.page.pageName}</a>
            </li>
        ));
    }

    return (
        <DownloadDoubleLayout titleKey="Print" bannerStyle="mini-header-download-print"
                              sections={filteredPrintSections}
                              renderSingleItem={renderSingleItem} icon='fa-file'/>
    )
}

export async function getServerSideProps({params}) {
    const printSections = await queryPageTitles(params.id);
    return {
        props: {
            printSections
        }
    }
}