import React, {useState} from "react";
import {queryVideos} from "../../../lib/graphQLClient";
import NormalPageTemplate from "../../../components/NormalPageTemplate";
import {useTranslation} from "react-i18next";
import DownloadAccordion from "../../../components/download/DownloadAccordion";
import {targetServer} from "../../../lib/apiConstants";


/**
 * Displays the video download files.
 * @param musicSections
 * @returns {JSX.Element}
 */
export default ({videoSections}) => {
    const {t} = useTranslation();

    console.log('videoSections', videoSections)

    const filteredVideoSectiosn = videoSections.filter(s => s.modules.filter(m => m.video).length > 0)

    function renderSingleItem(item) {
        return item?.modules?.filter(m => m.video).map((item, i) => {
            return <li key={i}>
                <i className="fa fa-download"/>
                {' '} <a href={`${targetServer}${item.video.url}`}
                         download={true}>{item.video.caption || item.video.name}</a>
            </li>
        });
    }

    return (
        <NormalPageTemplate titleKey={t("Video")} bannerStyle="mini-header-download-video">
            <div className="container">
                <h4 className="audioIntro">{t('download-video-intro')}</h4>
                <div className="row">
                    <div className="col-md-12">
                        <DownloadAccordion items={filteredVideoSectiosn} renderSingleItem={renderSingleItem} icon="fa-play-circle"/>
                    </div>
                </div>
            </div>
        </NormalPageTemplate>
    )
}

export async function getServerSideProps({params}) {
    const videoSections = await queryVideos(params.id);
    return {
        props: {
            videoSections
        }
    }
}