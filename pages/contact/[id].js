import React from "react";
import NormalPageTemplate from "../../components/NormalPageTemplate";
import {useTranslation} from "react-i18next";
import {queryCourseBaseInfo} from "../../lib/graphQLClient";
import ContactForm from "../../components/ContactForm";

/**
 * Displays the contact form
 * @param musicSections
 * @returns {JSX.Element}
 */
export default ({courseInfo}) => {
    const {t} = useTranslation();

    console.log('courseInfo', courseInfo)

    return (
        <NormalPageTemplate titleKey={t("Contact Us")} bannerStyle="mini-header-contact">
            <div className="container">
                <div className="row">
                    <ContactForm/>
                </div>
            </div>
        </NormalPageTemplate>
    )
}

export async function getServerSideProps({params}) {
    const courseInfo = await queryCourseBaseInfo(params.id);
    return {
        props: {
            courseInfo
        }
    }
}