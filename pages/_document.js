import Document, {Head, Html, Main, NextScript} from 'next/document'

/**
 * Used to add the Google Analytics tag.
 */
class MyDocument extends Document {
    static async getInitialProps(ctx) {
        const initialProps = await Document.getInitialProps(ctx)
        return {...initialProps}
    }

    render() {
        return (
            <Html>
                <Head>
                    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-37612976-2"></script>
                    <script
                        dangerouslySetInnerHTML={{
                            __html: `
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'UA-37612976-2');
            `,
                        }}
                    />
                </Head>
                <body className="panelsnap onepage flexheader">
                <Main/>
                <NextScript/>
                </body>
            </Html>
        )
    }
}

export default MyDocument