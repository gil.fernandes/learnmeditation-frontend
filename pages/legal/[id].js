import React, {useState} from "react";
import NormalPageTemplate from "../../components/NormalPageTemplate";
import {useTranslation} from "react-i18next";
import {queryLegal} from "../../lib/graphQLClient";

const DISCLAIMER = 0
const TERMS_AND_CONDITONS = 1
const PRIVACY_POLICY = 2

/**
 * Displays the about us form.
 * @param musicSections
 * @returns {JSX.Element}
 */
export default ({legalContent}) => {
    const {t} = useTranslation();

    const [displayItem, setDisplayItem] = useState(DISCLAIMER)

    console.log('queryLegal', legalContent)

    const chooseDispleyedItem = (course) => {
        switch (displayItem) {
            case DISCLAIMER:
                return course.disclaimer
            case TERMS_AND_CONDITONS:
                return course.terms_and_conditions
            case PRIVACY_POLICY:
                return course.privacy_policy
            default:
                return ""
        }
    }

    const chooseTitle = (course) => {
        switch (displayItem) {
            case DISCLAIMER:
                return t('Disclaimer')
            case TERMS_AND_CONDITONS:
                return t('Terms and conditions')
            case PRIVACY_POLICY:
                return t('Privacy Policy')
            default:
                return ""
        }
    }

    return (
        <NormalPageTemplate titleKey={t("Legal Information")} bannerStyle="mini-header-legal">
            <div className="container container-legal">
                <div className="row">
                    <aside className="col-md-3">
                        <div className="wm-album-nav wm-album-nav1 nav flex-column nav-pills nav-pills-custom"
                             id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <ul role="tablist" className="wm-tabs-wrap wm-tabs-wrap1">
                                <li className={displayItem === DISCLAIMER && 'active'} role="presentation">
                                    <a data-toggle="tab" role="tab" href="#disclaimer"
                                       aria-expanded="true" onClick={() => setDisplayItem(DISCLAIMER)}>{t('Disclaimer')}</a>
                                </li>
                                <li role="presentation" className={displayItem === TERMS_AND_CONDITONS && 'active'}>
                                    <a data-toggle="tab" role="tab" href="#terms-cond" aria-expanded="false"
                                       onClick={() => setDisplayItem(TERMS_AND_CONDITONS)}>{t('Terms and conditions')}</a>
                                </li>
                                <li role="presentation" className={displayItem === PRIVACY_POLICY && 'active'}>
                                    <a data-toggle="tab" role="tab" href="#policy"
                                       aria-expanded="false" onClick={() => setDisplayItem(PRIVACY_POLICY)}>{t('Privacy Policy')}</a>
                                </li>
                            </ul>
                        </div>
                    </aside>
                    <div className="col-md-9">
                        <img src="/img/lotus.png" className="lotus-small" />
                        <h3>{chooseTitle(legalContent?.course)}</h3>
                        <div dangerouslySetInnerHTML={{__html: chooseDispleyedItem(legalContent?.course)}}/>
                    </div>
                </div>
            </div>
        </NormalPageTemplate>
    )
}

export async function getServerSideProps({params}) {
    const legalContent = await queryLegal(params.id);
    return {
        props: {
            legalContent
        }
    }
}