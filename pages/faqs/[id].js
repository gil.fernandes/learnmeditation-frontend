import React, {useEffect} from 'react';
import {queryFaqs} from "../../lib/graphQLClient";
import NormalPageTemplate from "../../components/NormalPageTemplate";
import {useTranslation} from "react-i18next";
import {FAQAccordion} from "../../components/faqs/FAQAccordion";



/**
 * Displays the FAQs in an accordion.
 * @param faqSections
 * @returns {JSX.Element}
 */
export default ({faqSections}) => {

    const {t} = useTranslation();

    console.log('faqSections', faqSections)

    return (
        <NormalPageTemplate titleKey={faqSections?.faqcAtegory?.Name} bannerStyle="faq-header">
            <div className="container-xl">
                <FAQAccordion items={faqSections.faqcAtegory.faq_groups}/>
            </div>
        </NormalPageTemplate>
    )
}

export async function getServerSideProps({params}) {
    const faqSections = await queryFaqs(params.id);
    return {
        props: {
            faqSections
        }
    }
}