import React from "react";
import NormalPageTemplate from "../../components/NormalPageTemplate";
import {useTranslation} from "react-i18next";
import {queryAboutUsInfo, queryHelp, queryHowTo} from "../../lib/graphQLClient";
import marked from "marked";
import {targetServer} from "../../lib/apiConstants";


/**
 * Displays the help page.
 * @param helpContent
 * @returns {JSX.Element}
 */
export default ({helpContent}) => {
    const {t} = useTranslation();

    console.log('helpContent', helpContent)

    return (
        <NormalPageTemplate titleKey={t("Help")} bannerStyle="mini-header-help">
            <div className="container container-help">
                <div className="row">
                    <div className="col-md-12">
                        <div dangerouslySetInnerHTML={{__html: marked(helpContent?.course?.help)}}/>
                    </div>
                </div>
            </div>
        </NormalPageTemplate>
    )
}

export async function getServerSideProps({params}) {
    const helpContent = await queryHelp(params.id);
    return {
        props: {
            helpContent
        }
    }
}