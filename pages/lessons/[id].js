import React, {useContext, useEffect, useState} from 'react';
import {getSections} from "../../lib/learnMeditationServerFetch";
import {pageFetch, quizzFetch, sectionFetch} from "../../lib/learnMeditationClientFetch";

import {
    CourseContext,
    SET_COURSE_SECTIONS,
    SET_CURRENT_SECTION_INDEX,
    SET_SECTION_DATA
} from '../../context/CourseContext';
import NormalPageTemplate from "../../components/NormalPageTemplate";
import SectionsList from "../../components/lessons/SectionsList";
import Loading from "../../components/lessons/Loading";
import {isPage, isQuiz} from "../../lib/moduleConstants";
import LessonSlider from "../../components/lessons/LessonSlider";
import LessonsTitle from "../../components/lessons/LessonsTitle";
import {useTranslation} from "react-i18next";
import { Toast, ToastBody, ToastHeader } from 'reactstrap';
import InstructionsToast from "../../components/lessons/InstructionsToast";

export const extractLessonFromQuery = () => {
    const urlParams = new URLSearchParams(window.location.search);
    let lessonIndex = urlParams.get('lesson');
    if (lessonIndex) {
        lessonIndex = parseInt(lessonIndex) - 1
        return lessonIndex
    }
    return -1
}

/**
 * Used to display the lessons page main content.
 * @param sections The sections in the course
 * @param courseId The course id
 * @returns {JSX.Element}
 */
export default ({sections, courseId}) => {

    const {t} = useTranslation();

    const [lessonIndexSetFromUrl, setLessonIndexSetFromUrl] = useState(false)

    const {
        setCurrentSection,
        courseLanguageData, dispatch
    } = useContext(CourseContext);

    const sectionsAvailable = () => sections.length > 0;

    useEffect(() => {
        dispatch({type: SET_COURSE_SECTIONS, selectedCourseId: courseId, courseSections: sections})

        async function setModule() {
            if (sectionsAvailable()) {
                const lessonIndex = extractLessonFromQuery()
                const useFromUrl = lessonIndex > -1 && !lessonIndexSetFromUrl;
                if (useFromUrl) {
                    dispatch({type: SET_CURRENT_SECTION_INDEX, currentSectionIndex: lessonIndex})
                }
                const currentSectionIndex = useFromUrl ? lessonIndex : courseLanguageData.currentSectionIndex
                const localCurrentSection = sections[currentSectionIndex];
                setLessonIndexSetFromUrl(true)
                setCurrentSection(localCurrentSection);
                const sectionData = await sectionFetch(localCurrentSection.id);
                const moduleData = []
                // Filter the modules for accessibility
                sectionData.modules = sectionData.modules.filter(module => module.accessible)
                for (let i = 0, length = sectionData.modules.length; i < length; i++) {
                    const module = sectionData.modules[i];
                    if (isPage(module.courseModuleType)) {
                        moduleData.push(await pageFetch(module.page));
                    } else if (isQuiz(module.courseModuleType)) {
                        moduleData.push(await quizzFetch(module.quiz))
                    }
                }
                dispatch({type: SET_SECTION_DATA, sectionData, moduleData})
            }
        }

        setModule();
    }, [courseLanguageData.currentSectionIndex]);

    return (
        <>
            <NormalPageTemplate titleKey={t('Our Lessons')} bannerStyle="wm-mini-header-lesson">
                <InstructionsToast/>
                <LessonsTitle/>
                <SectionsList/>
                <Loading/>
                {!courseLanguageData.loadingLesson && <div className="col-md-12" id="lessonSliderMain">
                    <div className="tab-content">
                        <div role="tabpanel" className="tab-pane active" id="lesson1">
                            <div className="widget_slider">
                                <div className="text-center lsn-heading">
                                    <h2>{courseLanguageData?.sectionData?.name}</h2>
                                </div>
                            </div>
                            <LessonSlider courseId={courseId} sections={sections}/>
                        </div>
                    </div>
                </div>}
            </NormalPageTemplate>
        </>
    );
};

export async function getServerSideProps({params}) {
    const sections = await getSections(params.id);
    return {
        props: {
            sections,
            courseId: params.id
        }
    }
}