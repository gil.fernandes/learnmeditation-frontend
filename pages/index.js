import React, {useContext, useEffect} from 'react';
import {CourseContext} from "../context/CourseContext";
import Homepage from "../components/Homepage";
import {useTranslation} from "react-i18next";
import {getCurrentLanguage} from "../lib/langUtils";
import {findCourseForLang} from "../lib/langConfig";
import {queryVideoLink} from "../lib/graphQLClient";

export default () => {
    const {
        setCourseSections,
        setHomepageVideoLink,
        courseLanguageData
    } = useContext(CourseContext);

    const {i18n} = useTranslation();

    useEffect(() => {
        setCourseSections(null);
        const fetchVideoData = async () => {
            const currentCourseId = findCourseForLang(getCurrentLanguage(i18n))
            const videoData = await queryVideoLink(currentCourseId);
            setHomepageVideoLink(videoData.course.video_link)
        }
        fetchVideoData()
    }, [courseLanguageData]);

    return (
        <Homepage/>
    )
}

export async function getServerSideProps() {
    return {
        props: {
        }
    }
}