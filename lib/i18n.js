import i18n from "i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import {initReactI18next} from "react-i18next";
import React from "react";

i18n
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources: {
            en: {
                translations: {
                    "About Meditation": "About Meditation",
                    "About Us": "About Us",
                    "Audio": "Audio",
                    "Choose a Language ...": "Choose a Language ...",
                    "Choose your Meditation": "Choose your Meditation",
                    "Contact Us": "Contact Us",
                    "Contact Us Today": "Contact us today",
                    "Contact Us Extra": "",
                    "contact-mail-failed": "Contact mail submission failed. Please try again later.",
                    "contact-mail-success": "Contact mail sent successfully",
                    "Designed by": "Designed by",
                    "Disclaimer": "Disclaimer",
                    "Download": "Download",
                    "download-audio-intro": "Raj Yoga meditation classes in podcast, listen anytime, anywhere. You can also download them:",
                    "download-video-intro": "Here are some of the modules to experiment with:",
                    "Downloads": "Downloads",
                    "English": "English",
                    "FAQ": "FAQ",
                    "FAQ's": "FAQ's",
                    "French": "French",
                    "General FAQs": "General FAQs",
                    "German": "German",
                    "Help": "Help",
                    "hint-email": "Please enter your email address",
                    "hint-full-name": "Please enter your full name with at least {{amount_characters}} characters",
                    "hint-message": "Please enter a message with at least {{amount_characters}} characters",
                    "Homepage": "Homepage",
                    "How To": "How To",
                    "intro-p1": "Welcome to the Learn Meditation website, a free 7 lesson course in BK Meditation. This course is suitable for anyone wanting to try meditation for the first time and also for regular meditators interested in exploring another meditation method.",
                    "intro-p2": "There are many ways to meditate and people usually try a few methods until they find one that is right for them. Explore this course and see if BK Meditation is right for you. You don’t have to complete the whole course and there is no charge at any time.",
                    "intro-p3": "In this course you will find out about your soul. You will learn things that can enhance your life and your relationships, and improve your mental and emotional wellbeing.",
                    "intro-p4": "For a brief overview of the course, please watch the welcome video. We hope you enjoy BK Meditation and discover all the benefits that come from regular meditation practice.",
                    "Japanese": "Japanese",
                    "Know more": "Know more",
                    "Korean": "Korean",
                    "Legal Information": "Legal Information",
                    "Learn Meditation Online": "Learn Meditation Online",
                    "Lesson": "Lesson",
                    "Lessons": "Lessons",
                    "Meditation": "Meditation",
                    "Meditate": "Meditate",
                    "Meditate now": "Meditate now",
                    "meditate-now-intro": "These guided meditations will help you to assimilate the teaching through meditative journeys on each of the themes:",
                    "meditation-resources-intro": "Below you can find some more resources to help you meditate or to clarify some questions you might have about meditation.",
                    "Meta description": "Free Meditation, Meditation Methods, Meditation, How to Meditate, Meditations, Meditation music",
                    "My Email": "My Email",
                    "my-email-explanation": "* If you would like to send yourself the answers to these questions, just enter your email address and submit the form. This is an optional step.",
                    "Now": "Now",
                    "of": "of",
                    "Page": "Page",
                    "Pdf's and Audio's": "Pdf's and Audio's",
                    "Print": "Print",
                    "Privacy Policy": "Privacy Policy",
                    "Our 7 Lessons Course": "Our 7 Lessons Course",
                    "Our lessons": "Our lessons",
                    "Our Lessons": "Our Lessons",
                    "Resources": "Resources",
                    "Send responses to my email": "Send responses to my email",
                    "slider-our-lessons": "Our lessons",
                    "Submission failed": "Submission failed",
                    "Submit": "Submit",
                    "Submitting form, please wait ...": "Submitting form, please wait ...",
                    "Submission successful": "Submission successful",
                    "Swipe to see more results": "Swipe to see more testimonials",
                    "tagline": "Meditation is the path to quieten the mind and acquire inner strength.",
                    "Terms and conditions": "Terms and conditions",
                    "Testimonials": "Testimonials",
                    "twitter-hashtags": "meditation,well-being,relaxation,introspection",
                    "Usage Instructions": "Usage Instructions",
                    "usage-intruction-lessons": "Within each lesson, click on the arrows to the right of the text to move from page to page or just swipe the page left or right.<br />" +
                        "You can also <a href='{{audio_link}}'>listen</a> or <a href='{{download_link}}'>download</a> them.",
                    "User FAQs": "User FAQs",
                    "Video": "Video",
                    "Your E-mail": "Your E-mail",
                    "Your Message ...": "Your Message ...",
                    "Your Name": "Your Name"
                }
            },
            de: {
                translations: {
                    "About Meditation": "Über Meditation",
                    "About Us": "Über Uns",
                    "Audio": "Audio",
                    "Choose a Language ...": "Wähle eine Sprache ...",
                    "Choose your Meditation": "Wähle deine Meditation",
                    "Contact Us": "Kontakt",
                    "Contact Us Today": "Kontaktieren Sie uns jetzt",
                    "Contact Us Extra": ``,
                    "contact-mail-failed": "Übermittlung der Kontakt-E-Mail fehlgeschlagen. Bitte versuchen Sie es später erneut.",
                    "contact-mail-success": "Kontaktmail erfolgreich gesendet",
                    "Designed by": "Entworfen von",
                    "Disclaimer": "Haftungsausschluss",
                    "Download": "Herunterladen",
                    "download-audio-intro": "Raj Yoga Meditationskurse im Podcast, jederzeit und überall anhören. Sie können sie auch hier herunterladen:",
                    "download-video-intro": "Hier sind einige der Module zum Experimentieren:",
                    "Downloads": "Downloads",
                    "English": "Englisch",
                    "FAQ": "FAQ",
                    "FAQ's": "FAQ's",
                    "French": "Französisch",
                    "General FAQs": "Generelle FAQs",
                    "German": "Deutsch",
                    "Help": "Hilfe",
                    "hint-email": "Bitte geben Sie Ihre Email Adresse ein",
                    "hint-full-name": "Bitte geben Sie Ihren vollständigen Namen mit mindestens {{amount_characters}} Zeichen ein",
                    "hint-message": "Bitte geben Sie eine Nachricht mit mindestens {{amount_characters}} Zeichen ein",
                    "Homepage": "Startseite",
                    "How To": "Den Kurs Nutzen",
                    "intro-p1": "Willkommen zur Webseite „online meditieren lernen“, einem kostenfreien Kurs mit 7 Lektionen der Raja Yoga Meditation. Dieser Kurs ist für jeden geeignet, der Meditation zum ersten Mal ausprobieren möchte und auch für regelmäßig Meditierende, die daran interessiert sind, eine andere Meditationsmethode kennenzulernen.",
                    "intro-p2": "Es gibt viele Meditationsweisen und üblicherweise testen Menschen verschiedene Methoden bis sie diejenige finden, die zu ihnen passt. Erkunde diesen Kurs und sehe selbst, ob die Raja Yoga Meditation für Dich passend ist. Du musst den Kurs nicht vollständig durchführen und es fallen zu keiner Zeit Gebühren an.",
                    "intro-p3": "In diesem Kurs erfährst Du mehr über die Seele. Du lernst Dinge, die Dein Leben und Deine Beziehungen verbessern können, als auch Dein geistiges und emotionales Wohlbefinden.",
                    "intro-p4": "Für einen kurzen Überblick zu diesem Kurs, schaue bitte dieses Willkommensvideo an. Wir hoffen Du hast Freude mit der Raja Yoga Meditation und entdeckst all die Vorteile, die mit der regelmäßigen Übung entstehen.",
                    "Japanese": "Japanisch",
                    "Know more": "Mehr erfahren",
                    "Korean": "Koreanisch",
                    "Legal Information": "Impressum",
                    "Learn Meditation Online": "Learn Meditation Online",
                    "Lesson": "Lektion",
                    "Lessons": "Lektionen",
                    "Meditate": "Meditiere",
                    "Meditation": "Meditation",
                    "Meditate now": "Meditieren",
                    "meditate-now-intro": "Diese geführten Meditationen werden Ihnen helfen, die Lehren durch meditative Reisen zu jedem der Themen zu assimilieren:",
                    "meditation-resources-intro": "Im Folgenden finden Sie weitere Ressourcen, die Ihnen beim Meditieren helfen oder einige Fragen zur Meditation klären können.",
                    "Meta description": "Freie Meditation, Meditationsmethoden, Meditation, Meditieren, Meditationen, Meditationsmusik",
                    "My Email": "Meine Email",
                    "my-email-explanation": "* Wenn Sie sich die Antworten auf diese Fragen zusenden möchten, geben Sie einfach Ihre E-Mail-Adresse ein und senden Sie das Formular ab. Dies ist ein optionaler Schritt.",
                    "Now": "Jetzt",
                    "of": "von",
                    "Page": "Seite",
                    "Pdf's and Audio's": "Pdf und Audio",
                    "Print": "Print",
                    "Privacy Policy": "Datenschutz-Bestimmungen",
                    "Our 7 Lessons Course": "Unsere 7 Lektionen",
                    "Our lessons": "Unsere Lektionen",
                    "Our Lessons": "Unsere Lektionen",
                    "Resources": "Ressourcen",
                    "Send responses to my email": "Antworten auf meine E-Mail senden",
                    "slider-our-lessons": "Unsere Lektionen",
                    "Submission failed": "Einreichen fehlgeschlagen",
                    "Submit": "Senden",
                    "Submitting form, please wait ...": "Formular wird gesendet. Bitte warten ...",
                    "Submission successful": "Einreichung erfolgreich",
                    "Swipe to see more results": "Wischen Sie, um weitere Ergebnisse zu sehen",
                    "tagline": "Meditation ist ein wichtiger Weg, um den Geist zu reinigen und zu beruhigen.",
                    "Terms and conditions": "Allgemeine Geschäftsbedingungen",
                    "Testimonials": "Unsere Erfahrungen",
                    "twitter-hashtags": "Meditation,Wohlbefinden,Entspannung,Selbstbeobachtung",
                    "Usage Instructions": "Anleitung",
                    "usage-intruction-lessons": "Klicken Sie in jeder Lektion auf die Pfeile rechts neben dem Text, um von Seite zu Seite zu wechseln, oder wischen Sie einfach nach links oder rechts.<br />" +
                        "Sie können sich auch die Lektionen <a href='{{audio_link}}'>anhören</a> oder <a href='{{download_link}}'>herunterladen</a>.",
                    "Benutzer FAQs": "Benutzer FAQs",
                    "Video": "Video",
                    "Your E-mail": "Ihre E-mail",
                    "Your Message ...": "Ihre Nachricht ...",
                    "Your Name": "Ihr Name"
                }
            },
            fr: {
                translations: {
                    "About Meditation": "À propos de la méditation",
                    "About Us": "À propos",
                    "Audio": "L'audio",
                    "Choose a Language ...": "Choisissez une langue...",
                    "Choose your Meditation": "Choisissez votre méditation",
                    "Contact Us": "Contact",
                    "Contact Us Extra": `<h3>Pour des cours de méditation en Français, visitez le site de la&nbsp;
                    <a href="https://meditation-rajayoga.fr/" target="_blank">méditation du Raja Yoga en France</a>
                    &nbsp;ou au <a href="https://meditationquebec.org/cours-de-meditation-en-ligne/"
                             target="_blank">Québec</a></h3>`,
                    "Contact Us Today": "Contactez-nous aujourd'hui",
                    "contact-mail-failed": "L'envoi de l'e-mail de contact a échoué. Veuillez réessayer plus tard.",
                    "contact-mail-success": "E-mail de contact envoyé avec succès",
                    "Designed by": "Conçu par",
                    "Disclaimer": "Clause de non-responsabilité",
                    "Download": "Télécharger",
                    "download-audio-intro": "Les cours de méditation du Raj Yoga en podcast, à écouter à tout moment et n’importe où. Vous pouvez aussi les télécharger :",
                    "download-video-intro": "Voici quelques un des modules à expérimenter :",
                    "Downloads": "Downloads",
                    "English": "Anglais",
                    "FAQ": "FAQ",
                    "FAQ's": "FAQ's",
                    "French": "Français",
                    "General FAQs": "FAQ Générale",
                    "German": "Allemand",
                    "Help": "Aide",
                    "hint-email": "Veuillez saisir votre adresse e-mail",
                    "hint-full-name": "Veuillez entrer votre nom complet avec au moins {{amount_characters}} caractères",
                    "hint-message": "Veuillez saisir un message d'au moins {{amount_characters}} caractères",
                    "Homepage": "Page d'accueil",
                    "How To": "Comment utiliser ce cours",
                    "intro-p1": `Bienvenue sur le site “Apprendre à méditer” qui vous propose gratuitement d’apprendre à méditer en 7 jours.
Ce cours convient à ceux qui veulent essayer de méditer pour la première fois, comme à ceux qui méditent déjà régulièrement et qui aimeraient découvrir une autre forme de méditation.`,
                    "intro-p2": "Il existe de nombreuses façons de méditer et d’habitude, on essaye souvent plusieurs techniques avant de trouver celle qui nous convient. Découvrez ce cours et jugez si la méditation du Raja Yoga est faite pour vous. Il n’est pas nécessaire d’aller jusqu’au bout du cours et, à aucun moment, il ne vous sera demandé de payer quoi que ce soit.",
                    "intro-p3": "Ce cours parle de vous en tant qu’être spirituel, l’âme. Vous y trouverez des enseignements qui amélioreront votre qualité de vie, vos relations avec les autres, ainsi que votre état de santé mental et émotionnel.",
                    "intro-p4": `Pour avoir un aperçu du cours, vous pouvez écouter l'enregistrement proposé ci-contre.
Nous espérons que vous aimerez la méditation du Raja Yoga et découvrirez tous les bénéfices d’une pratique régulière de la méditation.`,
                    "Japanese": "Japonais",
                    "Know more": "Savoir plus",
                    "Korean": "Coréen",
                    "Legal Information": "Information légale",
                    "Learn Meditation Online": "Apprendre la méditation en ligne",
                    "Lesson": "Cours",
                    "Lessons": "Enseignements",
                    "Meditation": "Méditation",
                    "Meditate": "Méditer",
                    "Meditate now": "Méditez",
                    "meditate-now-intro": "Ces méditations guidées vous aideront à assimiler les enseignement à travers des voyages méditatifs sur chacune des thématiques :",
                    "meditation-resources-intro": "Vous trouverez ci-dessous d'autres ressources pour vous aider à méditer ou pour clarifier certaines questions que vous pourriez avoir sur la méditation.",
                    "Meta description": "Méditation gratuite, Méthodes de méditation, Méditation, Comment méditer, Méditations, Musique de méditation",
                    "My Email": "Mon email",
                    "my-email-explanation": "* Si vous souhaitez vous envoyer les réponses à ces questions, il vous suffit d'entrer votre adresse e-mail et de soumettre le formulaire. Ceci est une étape optionnelle.",
                    "Now": "Maintenant",
                    "of": "de",
                    "Page": "Page",
                    "Pdf's and Audio's": "PDF et audio",
                    "Print": "Imprimer",
                    "Privacy Policy": "Privacy Policy",
                    "Our 7 Lessons Course": "Nos enseignements en 7 cours",
                    "Our lessons": "Cours en ligne",
                    "Our Lessons": "Les Cours",
                    "Resources": "Ressources",
                    "Send responses to my email": "Envoyer des réponses à mon email",
                    "slider-our-lessons": "Les Cours",
                    "Submission failed": "Échec de la soumission",
                    "Submit": "Soumettre",
                    "Submitting form, please wait ...": "Envoi du formulaire, veuillez patienter...",
                    "Submission successful": "Soumission réussie",
                    "Swipe to see more results": "Faites glisser pour voir plus de témoignages",
                    "tagline": "La méditation est le chemin pour apaiser l'esprit et acquérir la force intérieure.",
                    "Terms and conditions": "Termes et conditions",
                    "Testimonials": "Témoignages",
                    "twitter-hashtags": "méditation, bien-être, détente, introspection",
                    "Usage Instructions": "Mode d'emploi",
                    "usage-intruction-lessons": "A l'intérieur de chaque cours, cliquez sur les flèches à droite du texte pour passer d’une page à l'autre.<br />" +
                        "Vous pouvez aussi <a href='{{audio_link}}'>écouter</a> les cours ou <a href='{{download_link}}'>les télécharger</a>.",
                    "User FAQs": "FAQ utilisateur",
                    "Video": "Vidéo",
                    "Your E-mail": "Votre e-mail",
                    "Your Message ...": "Votre message ...",
                    "Your Name": "Votre nom"
                }
            },
            sv: {
                translations: {
                    "About Meditation": "Om Meditation",
                    "About Us": "Om oss",
                    "Audio": "Ljud",
                    "Choose a Language ...": "Välj ett språk ...",
                    "Choose your Meditation": "Välj din meditation",
                    "Contact Us": "Kontakta oss",
                    "Contact Us Today": "Kontakta oss idag",
                    "Contact Us Extra": "",
                    "contact-mail-failed": "Det gick inte att skicka e-postmeddelandet. Försök igen senare.",
                    "contact-mail-success": "E-postmeddelandet skickades framgångsrikt",
                    "Designed by": "Designad av",
                    "Disclaimer": "Ansvarsfriskrivning",
                    "Download": "Ladda ner",
                    "download-audio-intro": "Raj Yoga-meditationsklasser som podcast, lyssna när som helst, var som helst. Du kan också ladda ner dem:",
                    "download-video-intro": "Här är några moduler att experimentera med:",
                    "Downloads": "Nedladdningar",
                    "English": "Engelska",
                    "FAQ": "Vanliga frågor",
                    "FAQ's": "Vanliga frågor",
                    "French": "Franska",
                    "General FAQs": "Allmänna frågor",
                    "German": "Tyska",
                    "Help": "Hjälp",
                    "hint-email": "Ange din e-postadress",
                    "hint-full-name": "Ange ditt fullständiga namn med minst {{amount_characters}} tecken",
                    "hint-message": "Ange ett meddelande med minst {{amount_characters}} tecken",
                    "Homepage": "Hemsida",
                    "How To": "Hur man gör",
                    "intro-p1": "Välkommen till webbplatsen Lär dig meditation, en gratis 7-lektioners kurs i BK Meditation. Denna kurs är lämplig för alla som vill prova meditation för första gången och även för regelbundna mediterare som är intresserade av att utforska en annan meditationsmetod.",
                    "intro-p2": "Det finns många sätt att meditera och människor brukar vanligtvis prova några metoder tills de hittar en som passar dem. Utforska den här kursen och se om BK Meditation passar dig. Du behöver inte slutföra hela kursen och det är aldrig någon kostnad.",
                    "intro-p3": "I den här kursen kommer du att få reda på mer om din själ. Du kommer att lära dig saker som kan förbättra ditt liv och dina relationer samt förbättra ditt mentala och emotionella välbefinnande.",
                    "intro-p4": "För en kort översikt över kursen, titta på välkomstvideon. Vi hoppas att du kommer att uppskatta BK Meditation och upptäcka alla fördelar som kommer med regelbunden meditationspraxis.",
                    "Japanese": "Japanska",
                    "Know more": "Läs mer",
                    "Korean": "Koreanska",
                    "Legal Information": "Juridisk information",
                    "Learn Meditation Online": "Lär dig meditation online",
                    "Lesson": "Lektion",
                    "Lessons": "Lektioner",
                    "Meditation": "Meditation",
                    "Meditate": "Meditation",
                    "Meditate now": "Meditation nu",
                    "meditate-now-intro": "Dessa guidade meditationer hjälper dig att integrera undervisningen genom meditativa resor för varje tema:",
                    "meditation-resources-intro": "Nedan hittar du ytterligare resurser som kan hjälpa dig att meditera eller klargöra eventuella frågor du kan ha om meditation.",
                    "Meta description": "Gratis meditation, meditationsmetoder, meditation, hur man mediterar, meditationer, meditationsmusik",
                    "My Email": "Min e-post",
                    "my-email-explanation": "* Om du vill skicka svaren på dessa frågor till dig själv, ange bara din e-postadress och skicka formuläret. Detta är ett frivilligt steg.",
                    "Now": "Nu",
                    "of": "av",
                    "Page": "Sida",
                    "Pdf's and Audio's": "Pdf-filer och ljudfiler",
                    "Print": "Skriv ut",
                    "Privacy Policy": "Integritetspolicy",
                    "Our 7 Lessons Course": "Vår 7-lektioners kurs",
                    "Our lessons": "Våra lektioner",
                    "Our Lessons": "Våra lektioner",
                    "Resources": "Resurser",
                    "Send responses to my email": "Skicka svar till min e-post",
                    "slider-our-lessons": "Våra lektioner",
                    "Submission failed": "Misslyckades att skicka",
                    "Submit": "Skicka",
                    "Submitting form, please wait ...": "Skickar formulär, vänta ...",
                    "Submission successful": "Skickades framgångsrikt",
                    "Swipe to see more results": "Svep för att se fler vittnesmål",
                    "tagline": "Meditation är vägen till att stilla sinnet och få inre styrka.",
                    "Terms and conditions": "Villkor och bestämmelser",
                    "Testimonials": "Vittnesmål",
                    "twitter-hashtags": "meditation,välmående,avslappning,introspektion",
                    "Usage Instructions": "Användningsanvisningar",
                    "usage-intruction-lessons": "Inom varje lektion kan du klicka på pilarna till höger om texten för att bläddra mellan sidorna eller svepa åt vänster eller höger på sidan.<br />" +
                        "Du kan också <a href='{{audio_link}}'>lyssna</a> eller <a href='{{download_link}}'>ladda ner</a> dem.",
                    "User FAQs": "Användar-FAQ",
                    "Video": "Video",
                    "Your E-mail": "Din e-post",
                    "Your Message ...": "Ditt meddelande ...",
                    "Your Name": "Ditt namn"
                }
            }
        },
        fallbackLng: "en",
        debug: true,

        // have a common namespace used around the full app
        ns: ["translations"],
        defaultNS: "translations",

        keySeparator: false, // we use content as keys

        interpolation: {
            escapeValue: false
        }
    });

export default i18n;