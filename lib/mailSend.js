import {targetServer} from "./apiConstants";

async function mailSend(mailContent) {
    return postToServer(mailContent, "/mail")
}

export async function contactMailSend(mailContent) {
    return postToServer(mailContent, "/mail/contact")
}

const postToServer = async (mailContent, location) => {
    const data = new FormData();
    data.append('data', JSON.stringify(mailContent));

    return await fetch(`${targetServer}${location}`, {
        method: 'post',
        body: data
    });
}

export default mailSend;