import {targetServer} from "./apiConstants";

async function fetchEntity(id, entityNamePlural) {
    const targetUrl = `${targetServer}/${entityNamePlural}/${id}`;
    const entity = await fetch(targetUrl);
    return await entity.json();
}

async function fetchAll(entityNamePlural) {
    let url = `${targetServer}/${entityNamePlural}`;
    console.info('Accessing ', url)
    const entities = await fetch(url);
    return await entities.json();
}

export const coursesFetch = async () => fetchAll('courses');

export const sectionFetch = async sectionId => fetchEntity(sectionId, 'sections');

export const pageFetch = async pageId => fetchEntity(pageId, 'pages');

export const quizzFetch = async quizzId => fetchEntity(quizzId, 'quizzes');

