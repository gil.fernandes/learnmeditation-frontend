import fetch from 'node-fetch';
import ApolloClient, {gql} from 'apollo-boost';
import {targetServer} from "./apiConstants";

export const client = new ApolloClient({
    fetch: fetch,
    uri: targetServer + '/graphql',
});

function filterModules(course, filterField = 'audio') {
    let o = course.sections;
    Object.values(o).forEach(s => {
        const keepIds = s['modules'].map((a, i) => {
            if (a[filterField]) {
                return i;
            }
        }).filter(a => a);
        console.log('keepIds', keepIds);
        s['audio_modules'] = keepIds.map(i => s['modules'][i]); // keep the audio modules in a separate reference.
    });
    return o;
}

const SECTIONS_SORT = "(sort: \"id\")"

export async function queryMeditations(id) {
    const meditations = await client.query({
        query: gql`
      {
        course(id: ${id}){
            sections ${SECTIONS_SORT} {
                id
                name
                modules {
                    id
                    audio {
                        id,
                        url,
                        name,
                        caption,
                        alternativeText
                    }
                }
            }
        }
    }
    `
    });
    let course = meditations.data.course;
    return filterModules(course);
}

export const queryVideos = async (id) => {
    const result = await client.query({
        query: gql`
        {
          course(id: ${id}) {
            sections ${SECTIONS_SORT} {
              id
              name
              modules {
                id
                video {
                  id
                  url
                  name
                  caption
                }
              }
            }
          }
        }
    `
    });
    let course = result.data.course;
    return filterModules(course, 'video');
}

export async function querySectionsOnly(id) {
    const meditations = await client.query({
        query: gql`
      {
        course(id: ${id}){
            sections ${SECTIONS_SORT} {
                id
                name
            }
        }
    }
    `
    });
    return meditations.data.course;
}

export async function querySectionById(id) {
    const section = await client.query({
        query: gql`
      {
        section(id: ${id}) {
            id
            name
            modules {
                id
                audio {
                    id,
                    url,
                    name,
                    caption,
                    alternativeText
                }
            }
        }
    }
    `
    });
    return section.data.section;
}

export async function queryQuizQuestions(id) {
    const questions = await client.query({
        query: gql`
        {
            quiz(id: ${id}) {
                id,
                questions {
                    name
                    qtype
                }
            }
        }
    `
    });
    return questions.data
}

export async function queryFaqs(id) {
    const questions = await client.query({
        query: gql`
        {
            faqcAtegory(id: ${id}) {
                id
                Name
                faq_groups {
                    id
                    Name
                    faqs {
                        Question
                        Answer
                    }
                }
            }
        }
    `
    });
    return questions.data
}

export async function queryPageTitles(id) {
    const questions = await client.query({
        query: gql`
            {
              course(id: ${id}) {
                sections ${SECTIONS_SORT} {
                  id
                  name
                  modules {
                    id
                    page {
                      id
                      pageName
                    }
                  }
                }
              }
            }
    `
    });
    return questions.data
}

export async function queryCourseBaseInfo(id) {
    const questions = await client.query({
        query: gql`
            {
              course(id: ${id}) {
                name
                summary
              }
            }
    `
    });
    return questions.data
}

export async function queryAboutUsInfo(id) {
    const courseInfo = await client.query({
        query: gql`
            {
              course(id: ${id}) {
                name
                about_us
                about_us_image {
                  url
                }
              }
            }
    `
    });
    return courseInfo.data
}

export async function queryHowTo(id) {
    const courseInfo = await client.query({
        query: gql`
            {
              course(id: ${id}) {
                name
                how_to
                image {
                  url
                }
              }
            }
    `
    });
    return courseInfo.data
}

export async function queryHelp(id) {
    const courseInfo = await client.query({
        query: gql`
            {
              course(id: ${id}) {
                name
                help
              }
            }
    `
    });
    return courseInfo.data
}

export async function queryLegal(id) {
    const courseInfo = await client.query({
        query: gql`
            {
              course(id: ${id}) {
                name
                disclaimer
                terms_and_conditions
                privacy_policy
              }
            }
    `
    });
    return courseInfo.data
}

export async function queryVideoLink(id) {
    const courseInfo = await client.query({
        query: gql`
            {
              course(id: ${id}) {
                video_link
              }
            }
    `
    });
    return courseInfo.data
}