
const env = process?.env

export const targetServer = env?.TARGET_SERVER || 'https://admin.learnmeditationonline.org';

export const titleImage = 'Title_BG_949d462db0.jpeg';

export const DEFAULT_COURSE = env?.DEFAULT_COURSE || 1
export const GERMAN_COURSE = env?.GERMAN_COURSE || 2
export const FRENCH_COURSE = env?.FRENCH_COURSE || 3
export const SWEDISH_COURSE = env?.SWEDISH_COURSE || 4