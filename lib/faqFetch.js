import {targetServer} from "./apiConstants";
import {
    DEFAULT_TESTIMONIAL_LANG_CODE,
    findCourseForLang,
    findGeneralFAQCategoryForLang,
    langConfig
} from "./langConfig";
import {queryFaqs, queryQuizQuestions} from "./graphQLClient";

const defaultFAQs = [
    {
        id: 2,
        Question: "I can’t concentrate in meditation.",
        Answer: "As with any new skill, regular practice will enhance your ability to concentrate. Try not to judge yourself harshly and accept where you are at today.",
        created_at: "2021-07-08T13:44:17.000Z",
        updated_at: "2021-07-08T13:47:45.000Z",
        faq_group: 1
    }
]

/**
 * Used to fetch testimonials.
 */
export const fetchFAQs = async (lang) => {
    const id = findGeneralFAQCategoryForLang(lang)
    return queryFaqs(id)
}