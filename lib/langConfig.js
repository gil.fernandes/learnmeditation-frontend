import {DEFAULT_COURSE, FRENCH_COURSE, GERMAN_COURSE, SWEDISH_COURSE} from "./apiConstants";

export const DEFAULT_TESTIMONIAL_LANG_CODE = 1;

const GERMAN_TESTIMONIALS_CODE = 7;

const FRENCH_TESTIMONIALS_CODE = 8;

const DEFAULT_GENERAL_FAQ = 1

const DEFAULT_USER_FAQ = 2

const GERMAN_GENERAL_FAQ = 3

export const LANGUAGES_WITH_FAQS = ['en', 'de']
export const LANGUAGES_WITH_HELP = ['en', 'de']
export const LANGUAGES_WITH_LEGAL = ['en', 'de', 'fr']

export const LANGUAGES = {
    ENGLISH: {
        CODE: 'en',
        NAME: 'English'
    },
    KOREAN: {
        CODE: 'ko',
        NAME: 'Korean'
    },
    JAPANESE: {
        CODE: 'jp',
        NAME: 'Japanese'
    },
    GERMAN: {
        CODE: 'de',
        NAME: 'German'
    },
    FRENCH: {
        CODE: 'fr',
        NAME: 'French'
    },
    SWEDISH: {
        CODE: 'sv',
        NAME: 'Swedish'
    }
}

/**
 * Configuration based on language.
 */
export const langConfig = {
    testimonialMap: {
        [LANGUAGES.ENGLISH.CODE]: DEFAULT_TESTIMONIAL_LANG_CODE,
        [LANGUAGES.GERMAN.CODE]: GERMAN_TESTIMONIALS_CODE,
        [LANGUAGES.FRENCH.CODE]: FRENCH_TESTIMONIALS_CODE,
        [LANGUAGES.SWEDISH.CODE]: FRENCH_TESTIMONIALS_CODE
    },
    courseMap: {
        [LANGUAGES.ENGLISH.CODE]: DEFAULT_COURSE,
        [LANGUAGES.GERMAN.CODE]: GERMAN_COURSE,
        [LANGUAGES.FRENCH.CODE]: FRENCH_COURSE,
        [LANGUAGES.SWEDISH.CODE]: SWEDISH_COURSE
    },
    generalFAQMap: {
        [LANGUAGES.ENGLISH.CODE]: DEFAULT_GENERAL_FAQ,
        [LANGUAGES.GERMAN.CODE]: GERMAN_GENERAL_FAQ,
        [LANGUAGES.FRENCH.CODE]: DEFAULT_GENERAL_FAQ,
        [LANGUAGES.SWEDISH.CODE]: DEFAULT_GENERAL_FAQ
    },
    userFAQMap: {
        [LANGUAGES.ENGLISH.CODE]: DEFAULT_USER_FAQ,
        [LANGUAGES.GERMAN.CODE]: GERMAN_GENERAL_FAQ,
        [LANGUAGES.FRENCH.CODE]: DEFAULT_USER_FAQ,
        [LANGUAGES.SWEDISH.CODE]: DEFAULT_USER_FAQ
    }
}

export const findTestimonialsForLang = (lang) => {
    let testimonialId = langConfig.testimonialMap[lang]
    return !testimonialId ? DEFAULT_TESTIMONIAL_LANG_CODE : testimonialId;
}

export const findCourseForLang = (lang) => {
    let courseId = langConfig.courseMap[lang]
    return !courseId ? DEFAULT_COURSE : courseId;
}

export const findGeneralFAQCategoryForLang = (lang) => {
    let id = langConfig.generalFAQMap[lang]
    return !id ? DEFAULT_GENERAL_FAQ : id;
}

export const findUserFAQCategoryForLang = (lang) => {
    let id = langConfig.userFAQMap[lang]
    return !id ? DEFAULT_USER_FAQ : id;
}