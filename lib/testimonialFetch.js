import {targetServer} from "./apiConstants";
import {DEFAULT_TESTIMONIAL_LANG_CODE, findCourseForLang, findTestimonialsForLang, langConfig} from "./langConfig";

const defaultTestimonials = [
    {
        picture: "http://bkwsu.org/media/lm/yoursay/michael_timmins.png",
        name: "Michael Timmins, Company Director, Sydney, Australia",
        message: "Meditation has not only helped me experience peace of mind but I have been able to integrate these benefits into my everyday life."
    },
    {
        picture: "http://bkwsu.org/media/lm/yoursay/carlos_molina.png",
        name: "Carlos Molina, Mechanical Engineer, Colombia",
        message: "Through BK meditation, I have improved my health a great deal and now perform my duties in a relaxed and peaceful way."
    },
    {
        picture: "http://bkwsu.org/media/lm/yoursay/keethan_nadaraja.png",
        name: "Keethan Nadarajah, Registered Nurse, Sydney, Australia",
        message: "Some people suggest that meditation may be boring, but for me, I learnt how to remain inspired from within, find my inner strength, be happy always and feel great no matter what."
    },
    {
        picture: "http://bkwsu.org/media/lm/yoursay/pam_engelshoven.png",
        name: "Pam van Engelshoven, Trainer, Curacao,Caribbean",
        message: "My daily meditation practice gives me so much peace and equilibrium that as a trainer I can act in a clear and relaxed way in any situation. This is the feedback I have received from trainees."
    },
    {
        picture: "http://bkwsu.org/media/lm/yoursay/mathias_steffen.png",
        name: "Mathias Steffen, Principal, Switzerland",
        message: "At one time I was losing happiness, strength and enthusiasm. The course helped me understand life’s laws, and the practice of meditation brought everything back."
    }
]

/**
 * Used to fetch testimonials.
 */
export const fetchTestimonials = async (lang) => {
    console.log('fetchTestimonials(lang)', lang)
    let langId = findTestimonialsForLang(lang)
    if(!langId) {
        langId = DEFAULT_TESTIMONIAL_LANG_CODE
    }
    const url = `${targetServer}/testimonials/${langId}`
    const entity = await fetch(url)
    const testimonialJson = await entity.json()
    if(testimonialJson?.languageTestimonials) {
        console.log('Using server testimonials', testimonialJson)
        return testimonialJson?.languageTestimonials
    }
    return defaultTestimonials
}