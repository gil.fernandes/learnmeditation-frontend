import {getSections} from "./learnMeditationServerFetch";
import {DEFAULT_COURSE, FRENCH_COURSE, GERMAN_COURSE, SWEDISH_COURSE, targetServer} from "./apiConstants";

const DEFAULT_LANG_CODE = DEFAULT_COURSE;

const langMap = {
    "en": DEFAULT_COURSE,
    "de": GERMAN_COURSE,
    "fr": FRENCH_COURSE,
    "sv": SWEDISH_COURSE
}

const defaultSlides = [
    {
        img: "img/m1.jpg",
        text: "Introduction to meditation"
    },
    {
        img: "img/m2.jpg",
        text: "Meditation and Soul Awareness"
    },
    {
        img: "img/m3.jpg",
        text: "Energising the Soul"
    },
    {
        img: "img/m4.jpg",
        text: "Meditation and Self Respect"
    },
    {
        img: "img/m5.jpg",
        text: "More about Meditation"
    },
    {
        img: "img/m6.jpg",
        text: "Eight Powers of the Soul"
    }
]


export const fetchLessonSlides = async (lang) => {

    try {
        let langId = langMap[lang] || DEFAULT_LANG_CODE;
        const sections = await getSections(langId)
        return sections.map((section) => {
            return {
                text: section.name,
                img: `${targetServer}${section.image?.url}`
            }
        });
    } catch(e) {
        console.error("Could not fetch slides", e)
        return defaultSlides
    }
}