

export const TYPE_PAGE = "PAGE";

export const TYPE_QUIZ = "QUIZ";

export const isPage = courseModuleType => courseModuleType === TYPE_PAGE;

export const isQuiz = courseModuleType => courseModuleType === TYPE_QUIZ;
