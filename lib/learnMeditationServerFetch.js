import fetch from 'node-fetch';
import {targetServer} from "./apiConstants";

export async function getSections(courseId) {
    console.log('${targetServer}/courses/${courseId}', `${targetServer}/courses/${courseId}` );
    const courseResponse = await fetch(`${targetServer}/courses/${courseId}`);
    const course = await courseResponse.json();
    return course.sections?.sort((a, b) => a.id - b.id);
}

export async function fetchDefaultCourse() {
    const defaultCourses = await fetch(`${targetServer}/courses?name_eq=Learn%20to%20Meditate`);
    if(defaultCourses) {
        const json = await defaultCourses.json();
        return json[0];
    }
    return [];
}