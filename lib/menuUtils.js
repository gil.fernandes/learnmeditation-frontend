/**
 * Used to setup the CSS changes based on the user scrolling on the menu
 */
export const setupMenuScrollBehaviour = () => {
    window.onscroll = function() {
        const doc = document.documentElement;
        const top = (window.pageYOffset || doc.scrollTop)  - (doc.clientTop || 0);

        const menuLessons = "menuLessons"
        const menuLessonsAnchors = ".menuLessons .menu-horizontal ul li a"
        const menuLessonsImage = ".menuHeader img"
        if (top > 100) {
            document.getElementsByClassName(menuLessons)[0].setAttribute('style', "background-color: #f6f9ff !important")
            document.querySelectorAll(menuLessonsAnchors)
                .forEach((n, i) => {
                    if (i > 0)
                        n.setAttribute('style', "color: #242020 !important")
                })
            document.querySelector(menuLessonsImage).setAttribute('src', '/img/logo.png')
        } else {
            if(document.getElementsByClassName(menuLessons) && document.getElementsByClassName(menuLessons).length > 0) {
                document.getElementsByClassName(menuLessons)[0].removeAttribute('style')
                document.querySelectorAll(menuLessonsAnchors)
                    .forEach(n => n.setAttribute('style', "color: white !important"))
                document.querySelectorAll(".menu-horizontal ul li .sub-menu a")
                    .forEach(n => n.setAttribute('style', "color: #242020 !important"))
                document.querySelector(menuLessonsImage).setAttribute('src', '/img/logo2.png')
            }
        }
    }
}