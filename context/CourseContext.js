import React, {createContext, useReducer, useState} from 'react';
import {findCourseForLang} from "../lib/langConfig";

export const CourseContext = createContext();

export const SET_LANGUAGE = "SET_LANGUAGE";

export const SET_SECTION_INFO = "SET_SECTION_INFO";

export const SET_COURSE_SECTIONS = "SET_COURSE_SECTIONS";

export const SET_SECTION_DATA = "SET_SECTION_DATA";

export const START_LOADING_LESSON = "START_LOADING_LESSON";

export const SET_CURRENT_SLIDE = "SET_CURRENT_SLIDE";

export const SET_CURRENT_SECTION_INDEX = "SET_CURRENT_SECTION_INDEX";

export const SET_USER_MAIL = "SET_USER_MAIL";

export const SET_FORM_SUBMISSION_START = "SET_FORM_SUBMISSION_START";

export const SET_FORM_SUBMISSION_STOP = "SET_FORM_SUBMISSION_STOP";

const reducer = (state, action) => {
    switch (action.type) {
        case SET_LANGUAGE:
            return {
                ...state,
                currentLanguage: action.currentLanguage,
                selectedCourseId: findCourseForLang(action.currentLanguage)
            };
        case SET_SECTION_INFO:
            return {
                ...state,
                sectionsInfo: action.sectionsInfo,
                loadingLesson: true
            };
        case SET_COURSE_SECTIONS:
            return {
                ...state,
                selectedCourseId: action.selectedCourseId,
                courseSections: action.courseSections
            };
        case SET_SECTION_DATA: {
            const lastSlideIndex = action.sectionData.modules.length - 1
            return {
                ...state,
                sectionData: action.sectionData,
                sectionModules: action.sectionData.modules,
                moduleData: action.moduleData,
                loadingLesson: false,
                currentSlide: state.showLast ? lastSlideIndex : 0
            };
        }
        case START_LOADING_LESSON:
            return {
                ...state,
                loadingLesson: true
            };
        case SET_CURRENT_SLIDE:
            return {
                ...state,
                currentSlide: action.currentSlide
            };
        case SET_CURRENT_SECTION_INDEX:
            return {
                ...state,
                currentSectionIndex: action.currentSectionIndex,
                loadingLesson: true,
                showLast: action.showLast
            }
        case SET_USER_MAIL:
            return {
                ...state,
                userMail: action.userMail
            }
        case SET_FORM_SUBMISSION_START:
            return {
                ...state,
                submitting: true
            }
        case SET_FORM_SUBMISSION_STOP:
            return {
                ...state,
                submitting: false
            }
    }
}

export const SET_CONTACT_FORM_NAME = "SET_CONTACT_FORM_NAME";

export const SET_CONTACT_FORM_EMAIL = "SET_CONTACT_FORM_EMAIL";

export const SET_CONTACT_FORM_MESSAGE = "SET_CONTACT_FORM_MESSAGE";

export const SET_CONTACT_FORM_SENDING = "SET_CONTACT_FORM_SENDING";

export const SET_CONTACT_FORM_SENDING_DONE = "SET_CONTACT_FORM_SENDING_DONE";

export const SET_CONTACT_FORM_ERROR_MESSAGE = "SET_CONTACT_FORM_ERROR_MESSAGE";

export const SET_CONTACT_FORM_SUCCESS_MESSAGE = "SET_CONTACT_FORM_SUCCESS_MESSAGE";

export const SET_CONTACT_FORM_CLEAR_MESSAGES = "SET_CONTACT_FORM_CLEAR_MESSAGES";

const contactFormReducer = (state, action) => {
    switch (action.type) {
        case SET_CONTACT_FORM_NAME:
            return {
                ...state,
                fullName: action.fullName
            }
        case SET_CONTACT_FORM_EMAIL:
            return {
                ...state,
                email: action.email
            }
        case SET_CONTACT_FORM_MESSAGE:
            return {
                ...state,
                message: action.message,
                errorMessage: '',
                successMessage: ''
            }
        case SET_CONTACT_FORM_SENDING:
            return {
                ...state,
                sending: true
            }
        case SET_CONTACT_FORM_SENDING_DONE:
            return {
                ...state,
                sending: false,
                message: ''
            }
        case SET_CONTACT_FORM_ERROR_MESSAGE:
            return {
                ...state,
                errorMessage: action.message,
                successMessage: ''
            }
        case SET_CONTACT_FORM_SUCCESS_MESSAGE:
            return {
                ...state,
                errorMessage: '',
                successMessage: action.message
            }
        case SET_CONTACT_FORM_CLEAR_MESSAGES:
            return {
                ...state,
                errorMessage: '',
                successMessage: ''
            }
    }
}

export const CourseProvider = props => {
    const [currentSection, setCurrentSection] = useState(null);
    const [nextSection, setNextSection] = useState(null);
    const [currentSectionData, setCurrentSectionData] = useState(null);
    const [currentModule, setCurrentModule] = useState(null);
    const [moduleCount, setModuleCount] = useState(null);
    const [currentModuleIndex, setCurrentModuleIndex] = useState(0);
    const [currentPage, setCurrentPage] = useState(null);
    const [currentQuizz, setCurrentQuizz] = useState(null);
    const [courseSections, setCourseSections] = useState(null);
    const [homepageVideoLink, setHomepageVideoLink] = useState(null);
    const [lessonDropDownShow, setLessonDropDownShow] = useState(false);
    const [meditateNowMeditations, setMeditateNowMeditations] = useState(false);
    const [meditateNowShow, setMeditateNowShow] = useState(false);
    const [meditateNowSubmenue, setMeditateNowSubmenue] = useState({});
    const [allCourses, setAllCourses] = useState([]);
    const [selectedCourseId, setSelectedCourseId] = useState([]);
    const [initialLanguage, setInitialLanguage] = useState("en")
    const [courseLanguageData, dispatch] = useReducer(
        reducer, {
            currentLanguage: "en",
            selectedCourseId: findCourseForLang("en"),
            sectionsInfo: [],
            courseSections: [],
            sectionData: [],
            sectionModules: [],
            loadingLesson: true,
            currentSlide: 0,
            currentSectionIndex: 0,
            showLast: false,
            userMail: "",
            submitting: false
        }
    )
    const [contactFormData, dispatchContactFormData] = useReducer(
        contactFormReducer,
        {
            fullName: "",
            email: "",
            message: "",
            sending: false,
            errorMessage: '',
            successMessage: ''
        }
    )
    return (
        <CourseContext.Provider value={{
            currentSection, setCurrentSection,
            nextSection, setNextSection, currentSectionData, setCurrentSectionData,
            currentModule, setCurrentModule, moduleCount, setModuleCount,
            currentModuleIndex, setCurrentModuleIndex,
            currentPage, setCurrentPage,
            currentQuizz, setCurrentQuizz,
            courseSections, setCourseSections,
            homepageVideoLink, setHomepageVideoLink,
            lessonDropDownShow, setLessonDropDownShow,
            meditateNowMeditations, setMeditateNowMeditations,
            meditateNowShow, setMeditateNowShow,
            meditateNowSubmenue, setMeditateNowSubmenue,
            allCourses, setAllCourses,
            selectedCourseId, setSelectedCourseId,
            initialLanguage, setInitialLanguage,
            courseLanguageData, dispatch,
            contactFormData, dispatchContactFormData
        }}>
            {props.children}
        </CourseContext.Provider>
    )
};